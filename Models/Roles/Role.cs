﻿using System;
using System.Collections.Generic;
using System.Text;
using AspNetCore.Identity.Mongo.Model;
using MongoDB.Driver;

namespace Models.Roles
{
    public class Role : MongoRole
    {
        public Role() : base() { }

        public Role(string name) : base(name)
        {
        }
    }
}
