﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Roles
{
    public class RoleUserPatchInfo
    {
        public string UserId { get; }
        public string UserRole { get; set; }

        public RoleUserPatchInfo(string userId, string userRole = null)
        {
            UserId = userId;
            UserRole = userRole;
        }
    }
}
