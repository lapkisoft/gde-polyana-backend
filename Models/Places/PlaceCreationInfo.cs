using System;
using System.Collections.Generic;
using System.Linq;

namespace Models.Places
{
    public class PlaceCreationInfo
    {
        public string Id { get; }
        public string Name { get; }
        public string Description { get; }
        public string Address { get; }
        public IReadOnlyList<double> Coordinates { get; }
        public string Phone { get; }
        public IReadOnlyList<string> Types { get; }
        public IReadOnlyList<string> Cuisines { get; }
        public PlaceAvgCheck AvgCheck { get; }
        public IReadOnlyList<PlaceWorkingTime> WorkingTime { get; }
        public string Manager { get; }
        
        public PlaceCreationInfo(string id, string name, string description, string address, 
            IEnumerable<double> coordinates, string phone, IEnumerable<string> types, IEnumerable<string> cuisines, 
            PlaceAvgCheck? avgCheck, IEnumerable<PlaceWorkingTime> workingTime, string manager)
        {
            Id = id?.ToLower() ?? throw new ArgumentNullException(nameof(id));
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Description = description ?? string.Empty;
            Address = address ?? throw new ArgumentNullException(nameof(address));
            Coordinates = coordinates?.ToArray() ?? throw new ArgumentNullException(nameof(coordinates));
            Phone = phone ?? throw new ArgumentNullException(nameof(phone));
            Types = types?.ToArray() ?? throw new ArgumentNullException(nameof(types));
            Cuisines = cuisines?.ToArray() ?? new string[0];
            AvgCheck = avgCheck ?? throw new ArgumentNullException(nameof(avgCheck));
            WorkingTime = workingTime?.ToArray() ?? throw new ArgumentNullException(nameof(workingTime));
            Manager = manager ?? throw new ArgumentNullException(nameof(manager));
        }
    }
}