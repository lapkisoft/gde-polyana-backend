using System;
using System.Collections.Generic;

namespace Models.Places
{
    public class PlacePatchInfo
    {
        public string Id { get; }
        public string Name { get; set; }
        public string Avatar { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public IReadOnlyList<double> Coordinates { get; set; }
        public string Phone { get; set; }
        public IReadOnlyList<string> Types { get; set; }
        public IReadOnlyList<string> Cuisines { get; set; }
        public PlaceAvgCheck? AvgCheck { get; set; }
        public IReadOnlyList<PlaceWorkingTime> WorkingTime { get; set; }

        public PlacePatchInfo(string id, string name = null, string avatar = null, string description = null, 
            string address = null, IReadOnlyList<double> coordinates = null, string phone = null, 
            IReadOnlyList<string> types = null, IReadOnlyList<string> cuisines = null, PlaceAvgCheck? avgCheck = null,
            IReadOnlyList<PlaceWorkingTime> workingTime = null)
        {
            Id = id;
            Name = name;
            Avatar = avatar;
            Description = description;
            Address = address;
            Coordinates = coordinates;
            Phone = phone;
            Types = types;
            Cuisines = cuisines;
            AvgCheck = avgCheck;
            WorkingTime = workingTime;
        }
    }
}