using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Models.Places.Repositories
{
    public interface IPlaceRepository
    {
        Task<Place> CreateAsync(PlaceCreationInfo creationInfo, CancellationToken cancellationToken);
        Task<IReadOnlyList<Place>> SearchAsync(PlaceSearchInfo searchInfo, CancellationToken cancellationToken);
        Task<Place> GetAsync(string id, CancellationToken cancellationToken);
        Task<Place> GetByTokenAsync(Guid telegramToken, CancellationToken cancellationToken);
        Task<Place> PatchAsync(PlacePatchInfo patchInfo, CancellationToken cancellationToken);
        Task RemoveAsync(string id, CancellationToken cancellationToken);
    }
}