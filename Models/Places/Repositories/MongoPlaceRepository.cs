using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Models.Places.Exceptions;
using MongoDB.Driver;

namespace Models.Places.Repositories
{
    public class MongoPlaceRepository : IPlaceRepository
    {
        private readonly IMongoCollection<Place> places;

        public MongoPlaceRepository()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("GdePolyanaDb");
            places = database.GetCollection<Place>("Places");
        }
        
        public Task<Place> CreateAsync(PlaceCreationInfo creationInfo, CancellationToken cancellationToken)
        {
            if (creationInfo == null)
            {
                throw new ArgumentNullException(nameof(creationInfo));
            }

            cancellationToken.ThrowIfCancellationRequested();
            var placeWithSameId = places.Find(place => place.Id == creationInfo.Id).FirstOrDefault();

            if (placeWithSameId != null)
            {
                throw new PlaceDuplicationException(creationInfo.Id);
            }
            
            var now = DateTime.UtcNow;
            var telegramToken = Guid.NewGuid();
            var newPlace = new Place
            {
                Id = creationInfo.Id,
                Name = creationInfo.Name,
                Avatar = string.Empty,
                Description = creationInfo.Description,
                Address = creationInfo.Address,
                Coordinates = creationInfo.Coordinates,
                Phone = creationInfo.Phone,
                Types = creationInfo.Types,
                Cuisines = creationInfo.Cuisines,
                AvgCheck = creationInfo.AvgCheck,
                WorkingTime = creationInfo.WorkingTime,
                Manager = creationInfo.Manager,
                TelegramToken = telegramToken,
                CreatedAt = now,
                LastUpdateAt = now
            };

            places.InsertOne(newPlace, cancellationToken: cancellationToken);
            return Task.FromResult(newPlace);
        }

        public Task<IReadOnlyList<Place>> SearchAsync(PlaceSearchInfo searchInfo, CancellationToken cancellationToken)
        {
            if (searchInfo == null)
            {
                throw new ArgumentNullException(nameof(searchInfo));
            }
            
            cancellationToken.ThrowIfCancellationRequested();
            
            var search = places.Find(item => true).ToEnumerable();

            if (searchInfo.Types != null)
            {
                search = search.Where(item => item.Types.Any(type => searchInfo.Types.Contains(type)));
            }

            if (searchInfo.Cuisines != null)
            {
                search = search.Where(item => item.Cuisines.Any(cuisine => searchInfo.Cuisines.Contains(cuisine)));
            }

            if (searchInfo.AvgChecks != null)
            {
                search = search.Where(item => searchInfo.AvgChecks.Contains(item.AvgCheck));
            }

            if (searchInfo.Manager != null)
            {
                search = search.Where(item => searchInfo.Manager.Equals(item.Manager));
            }

            if (searchInfo.Offset != null)
            {
                search = search.Skip(searchInfo.Offset.Value);
            }

            if (searchInfo.Limit != null)
            {
                search = search.Take(searchInfo.Limit.Value);
            }

            var result = search.ToList();
            return Task.FromResult<IReadOnlyList<Place>>(result);
        }

        public Task<Place> GetAsync(string id, CancellationToken cancellationToken)
        {
            if (id == null)
            {
                throw new ArgumentNullException(nameof(id));
            }

            cancellationToken.ThrowIfCancellationRequested();
            var place = places.Find(item => item.Id == id).FirstOrDefault();

            if (place == null)
            {
                throw new PlaceNotFoundException(id);
            }

            return Task.FromResult(place);
        }

        public Task<Place> GetByTokenAsync(Guid telegramToken, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            
            var place = places.Find(item => item.TelegramToken == telegramToken).FirstOrDefault();
            
            if (place == null)
            {
                throw new PlaceNotFoundException(string.Empty);
            }

            return Task.FromResult(place);
        }

        public Task<Place> PatchAsync(PlacePatchInfo patchInfo, CancellationToken cancellationToken)
        {
            if (patchInfo == null)
            {
                throw new ArgumentNullException(nameof(patchInfo));
            }

            cancellationToken.ThrowIfCancellationRequested();
            var place = places.Find(item => item.Id == patchInfo.Id).FirstOrDefault();

            if (place == null)
            {
                throw new PlaceNotFoundException(patchInfo.Id);
            }

            var updated = false;

            if (patchInfo.Name != null)
            {
                place.Name = patchInfo.Name;
                updated = true;
            }
            
            if (patchInfo.Avatar != null)
            {
                place.Avatar = patchInfo.Avatar;
                updated = true;
            }
            
            if (patchInfo.Description != null)
            {
                place.Description = patchInfo.Description;
                updated = true;
            }

            if (patchInfo.Address != null)
            {
                place.Address = patchInfo.Address;
                updated = true;
            }

            if (patchInfo.Coordinates != null)
            {
                place.Coordinates = patchInfo.Coordinates;
                updated = true;
            }

            if (patchInfo.Phone != null)
            {
                place.Phone = patchInfo.Phone;
                updated = true;
            }

            if (patchInfo.Types != null && patchInfo.Types.Any())
            {
                place.Types = patchInfo.Types;
                updated = true;
            }

            if (patchInfo.Cuisines != null)
            {
                place.Cuisines = patchInfo.Cuisines;
                updated = true;
            }

            if (patchInfo.AvgCheck != null)
            {
                place.AvgCheck = (PlaceAvgCheck)patchInfo.AvgCheck;
                updated = true;
            }

            if (patchInfo.WorkingTime != null)
            {
                place.WorkingTime = patchInfo.WorkingTime;
                updated = true;
            }

            if (updated)
            {
                place.LastUpdateAt = DateTime.UtcNow;
                places.ReplaceOne(item => item.Id == patchInfo.Id, place);
            }

            return Task.FromResult(place);
        }

        public Task RemoveAsync(string id, CancellationToken cancellationToken)
        {
            if (id == null)
            {
                throw new ArgumentNullException(nameof(id));
            }
            
            cancellationToken.ThrowIfCancellationRequested();
            var deleteResult = places.DeleteOne(place => place.Id == id);
            
            if (deleteResult.DeletedCount == 0)
            {
                throw new PlaceNotFoundException(id);
            }

            return Task.CompletedTask;
        }
    }
}