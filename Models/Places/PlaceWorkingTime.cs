using System;

namespace Models.Places
{
    public class PlaceWorkingTime
    {
        public TimeSpan OpenTime { get; set; }
        public TimeSpan CloseTime { get; set; }
    }
}