﻿using System;

namespace Models.Places.Exceptions
{
    public class PlaceDuplicationException : Exception
    {
        /// <summary>
        /// Создает новый экземпляр исключения о том, что заведение существует.
        /// </summary>
        /// <param name="placeId">Заведение с указанным идентификатором уже существует.</param>
        public PlaceDuplicationException(string placeId)
            : base($"Place \"{placeId}\" already exists.")
        {
        }
    }
}