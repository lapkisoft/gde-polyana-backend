﻿using System;

namespace Models.Places.Exceptions
{
    public class PlaceNotFoundException : Exception
    {
        /// <summary>
        /// Создает новый экземпляр исключения о том, что заведение не найдено.
        /// </summary>
        /// <param name="placeId">Заведение с указанным идентификатором не найдено.</param>
        public PlaceNotFoundException(string placeId)
            : base($"Place \"{placeId}\" is not found.")
        {
        }
    }
}
