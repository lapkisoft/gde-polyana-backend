namespace Models.Places
{
    public enum PlaceAvgCheck
    {
        OneDollar,
        TwoDollars,
        ThreeDollars
    }
}