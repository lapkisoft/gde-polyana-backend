namespace Models.Places
{
    public class PlaceSearchInfo
    {
        /// <summary>
        /// Позиция, начиная с которой нужно производить поиск
        /// </summary>
        public int? Offset { get; set; }

        /// <summary>
        /// Количество заведений, которое нужно вернуть
        /// </summary>
        public int? Limit { get; set; }

        /// <summary>
        /// Типы заведений
        /// </summary>
        public string[] Types { get; set; }

        /// <summary>
        /// Средний чек
        /// </summary>
        public PlaceAvgCheck?[] AvgChecks { get; set; }
        
        /// <summary>
        /// Кухня
        /// </summary>
        public string[] Cuisines { get; set; }
        
        /// <summary>
        /// Идентификатор пользователя, управляющего заведением
        /// </summary>
        public string Manager { get; set; }
    }
}