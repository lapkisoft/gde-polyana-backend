using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Models.Places
{
    public class Place
    {
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        public string Id { get; set; }
        
        [BsonElement("Name")]
        [BsonRepresentation(BsonType.String)]
        public string Name { get; set; }
        
        [BsonElement("Avatar")]
        [BsonRepresentation(BsonType.String)]
        public string Avatar { get; set; }
        
        [BsonElement("Description")]
        [BsonRepresentation(BsonType.String)]
        public string Description { get; set; }
        
        [BsonElement("Address")]
        [BsonRepresentation(BsonType.String)]
        public string Address { get; set; }
        
        [BsonElement("Coordinates")]
        public IReadOnlyList<double> Coordinates { get; set; }
        
        [BsonElement("Phone")]
        [BsonRepresentation(BsonType.String)]
        public string Phone { get; set; }
        
        [BsonElement("Types")]
        public IReadOnlyList<string> Types { get; set; }

        [BsonElement("Cuisines")]
        public IReadOnlyList<string> Cuisines { get; set; }

        [BsonElement("AvgCheck")]
        [BsonRepresentation(BsonType.String)]
        public PlaceAvgCheck AvgCheck { get; set; }
        
        [BsonElement("WorkingTime")]
        public IReadOnlyList<PlaceWorkingTime> WorkingTime { get; set; }
        
        [BsonElement("Manager")]
        [BsonRepresentation(BsonType.String)]
        public string Manager { get; set; }
        
        [BsonElement("TelegramToken")]
        [BsonRepresentation(BsonType.String)]
        public Guid TelegramToken { get; set; }
        
        [BsonElement("CreatedAt")]
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime CreatedAt { get; set; }
        
        [BsonElement("LastUpdateAt")]
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime LastUpdateAt { get; set; }
    }
}