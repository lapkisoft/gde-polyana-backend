using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Models.PlaceTypes
{
    public class PlaceType
    {
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        public string Id { get; set; }
        
        [BsonElement("Name")]
        [BsonRepresentation(BsonType.String)]
        public string Name { get; set; }
        
        [BsonElement("Count")]
        [BsonRepresentation(BsonType.Int32)]
        public int Count { get; set; }
    }
}