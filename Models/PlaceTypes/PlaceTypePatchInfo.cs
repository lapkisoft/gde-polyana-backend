namespace Models.PlaceTypes
{
    public class PlaceTypePatchInfo
    {
        public string Id { get; }
        public string Name { get; set; }

        public PlaceTypePatchInfo(string id, string name = null)
        {
            Id = id;
            Name = name;
        }
    }
}