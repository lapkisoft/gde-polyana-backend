using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Models.PlaceTypes.Exceptions;
using MongoDB.Driver;

namespace Models.PlaceTypes.Repositories
{
    public class MongoPlaceTypeRepository : IPlaceTypeRepository
    {
        private readonly IMongoCollection<PlaceType> placeTypes;

        public MongoPlaceTypeRepository()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("GdePolyanaDb");
            placeTypes = database.GetCollection<PlaceType>("PlaceTypes");
        }

        public Task<PlaceType> CreateAsync(PlaceTypeCreationInfo creationInfo, CancellationToken cancellationToken)
        {
            if (creationInfo == null)
            {
                throw new ArgumentNullException(nameof(creationInfo));
            }

            cancellationToken.ThrowIfCancellationRequested();
            var placeTypeWithSameId = placeTypes.Find(item => item.Id == creationInfo.Id).FirstOrDefault();

            if (placeTypeWithSameId != null)
            {
                throw new PlaceTypeDuplicationException(creationInfo.Id);
            }

            var placeType = new PlaceType
            {
                Id = creationInfo.Id,
                Name = creationInfo.Name,
                Count = 0
            };

            placeTypes.InsertOne(placeType);
            return Task.FromResult(placeType);
        }

        public Task<PlaceType> GetAsync(string id, CancellationToken cancellationToken)
        {
            if (id == null)
            {
                throw new ArgumentNullException(nameof(id));
            }
            
            cancellationToken.ThrowIfCancellationRequested();
            var placeType = placeTypes.Find(item => item.Id == id).FirstOrDefault();
            
            if (placeType == null)
            {
                throw new PlaceTypeNotFoundException(id);
            }

            return Task.FromResult(placeType);
        }
        
        public Task<IReadOnlyList<PlaceType>> GetAllAsync(CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var types = placeTypes.Find(item => true).ToList();
            return Task.FromResult<IReadOnlyList<PlaceType>>(types);
        }

        public Task<PlaceType> PatchAsync(PlaceTypePatchInfo patchInfo, CancellationToken cancellationToken)
        {
            if (patchInfo == null)
            {
                throw new ArgumentNullException(nameof(patchInfo));
            }

            cancellationToken.ThrowIfCancellationRequested();
            var placeType = placeTypes.Find(item => item.Id == patchInfo.Id).FirstOrDefault();

            if (placeType == null)
            {
                throw new PlaceTypeNotFoundException(patchInfo.Id);
            }

            if (string.IsNullOrEmpty(patchInfo.Name))
            {
                return Task.FromResult(placeType);
            }
            
            placeType.Name = patchInfo.Name;
            placeTypes.ReplaceOne(item => item.Id == patchInfo.Id, placeType);
            return Task.FromResult(placeType);
        }

        public Task RemoveAsync(string id, CancellationToken cancellationToken)
        {
            if (id == null)
            {
                throw new ArgumentNullException(nameof(id));
            }

            cancellationToken.ThrowIfCancellationRequested();
            var deleteResult = placeTypes.DeleteOne(type => type.Id == id);

            if (deleteResult.DeletedCount == 0)
            {
                throw new PlaceTypeNotFoundException(id);
            }

            return Task.CompletedTask;
        }
        
        public Task ChangeCountersAsync(IReadOnlyList<string> ids, CounterOperations operation, CancellationToken cancellationToken)
        {
            if (ids == null)
            {
                throw new ArgumentNullException(nameof(ids));
            }

            cancellationToken.ThrowIfCancellationRequested();
            var updatedPlaceTypes = placeTypes.Find(item => ids.Contains(item.Id)).ToList();

            foreach (var placeType in updatedPlaceTypes)
            {
                if (operation == CounterOperations.Increment)
                {
                    placeType.Count++;                    
                }
                else
                {
                    placeType.Count--;
                }
                
                placeTypes.ReplaceOne(item => item.Id == placeType.Id, placeType);
            }

            return Task.CompletedTask;
        }
    }
}