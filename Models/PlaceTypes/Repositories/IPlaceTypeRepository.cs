using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Models.PlaceTypes.Repositories
{
    public interface IPlaceTypeRepository
    {
        Task<PlaceType> CreateAsync(PlaceTypeCreationInfo creationInfo, CancellationToken cancellationToken);
        Task<PlaceType> GetAsync(string id, CancellationToken cancellationToken);
        Task<IReadOnlyList<PlaceType>> GetAllAsync(CancellationToken cancellationToken);
        Task<PlaceType> PatchAsync(PlaceTypePatchInfo patchInfo, CancellationToken cancellationToken);
        Task RemoveAsync(string id, CancellationToken cancellationToken);
        Task ChangeCountersAsync(IReadOnlyList<string> ids, CounterOperations operation, CancellationToken cancellationToken);
    }
}