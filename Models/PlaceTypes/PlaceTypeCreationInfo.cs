using System;

namespace Models.PlaceTypes
{
    public class PlaceTypeCreationInfo
    {
        public string Id { get; }
        public string Name { get; }

        public PlaceTypeCreationInfo(string id, string name)
        {
            Id = id?.ToLower() ?? throw new ArgumentNullException(nameof(id));
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }
    }
}