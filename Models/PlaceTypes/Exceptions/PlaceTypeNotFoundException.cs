﻿using System;

namespace Models.PlaceTypes.Exceptions
{
    public class PlaceTypeNotFoundException : Exception
    {
        /// <summary>
        /// Создает новый экземпляр исключения о том, что тип заведения не найден.
        /// </summary>
        /// <param name="placeTypeId">Тип заведения с указанным идентификатором не найден.</param>
        public PlaceTypeNotFoundException(string placeTypeId)
            : base($"PlaceType \"{placeTypeId}\" is not found.")
        {
        }
    }
}
