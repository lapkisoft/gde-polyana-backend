﻿using System;

namespace Models.PlaceTypes.Exceptions
{
    public class PlaceTypeDuplicationException : Exception
    {
        /// <summary>
        /// Создает новый экземпляр исключения о том, что тип заведения уже существует.
        /// </summary>
        /// <param name="placeTypeId">Тип заведения с указанным идентификатором уже существует.</param>
        public PlaceTypeDuplicationException(string placeTypeId)
            : base($"PlaceType \"{placeTypeId}\" already exists.")
        {
        }
    }
}

