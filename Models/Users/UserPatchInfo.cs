﻿using System;
using System.Collections.Generic;
using System.Text;
using Models.Roles;

namespace Models.Users
{
    public class UserPatchInfo
    {
        public string Username { get; }
        public string OldPassword { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public bool? IsManagerRoleRequested { get; set; }

        public UserPatchInfo(string username, string oldPassword = null, string password = null,
            string confirmPassword = null, bool? isManagerRoleRequested = null )
        {
            Username = username;
            OldPassword = oldPassword;
            Password = password;
            ConfirmPassword = confirmPassword;
            IsManagerRoleRequested = isManagerRoleRequested;
        }
    }
}
