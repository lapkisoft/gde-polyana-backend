namespace Models
{
    public enum CounterOperations
    {
        Increment,
        Decrement
    }
}