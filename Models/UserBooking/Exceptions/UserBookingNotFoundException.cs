﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.UserBooking.Exceptions
{
    public class UserBookingNotFoundException : Exception
    {
        public UserBookingNotFoundException(string userBookingId)
            : base($"UserBooking \"{userBookingId}\" is not found.")
        {
        }
    }
}
