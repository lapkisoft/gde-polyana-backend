﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.UserBooking
{
    public class UserBookingPatchInfo
    {
        public Guid Id { get; }
        public DateTime? OrderTime { get; set; }
        public int? NumberOfPersons { get; set; }
        public BookingStatus? Status { get; set; }

        public UserBookingPatchInfo(Guid id, 
            DateTime? orderTime = null, int? numberOfPersons = null, BookingStatus? status = null)
        {
            Id = id;
            OrderTime = orderTime;
            NumberOfPersons = numberOfPersons;
            Status = status;
        }
    }
}
