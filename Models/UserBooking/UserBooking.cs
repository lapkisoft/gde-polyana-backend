﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Models.UserBooking
{
    public class UserBooking
    {
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        public Guid Id { get; set; }
        
        [BsonElement("PlaceId")]
        [BsonRepresentation(BsonType.String)]
        public string PlaceId { get; set; }
        
        [BsonElement("PlaceName")]
        [BsonRepresentation(BsonType.String)]
        public string PlaceName { get; set; }
        
        [BsonElement("UserId")]
        [BsonRepresentation(BsonType.String)]
        public string UserId { get; set; }
        
        [BsonElement("UserPhone")]
        [BsonRepresentation(BsonType.String)]
        public string UserPhone { get; set; }

        [BsonElement("OrderTime")]
        [BsonRepresentation(BsonType.String)]
        public DateTime OrderDateTime { get; set; }

        [BsonElement("Status")]
        [BsonRepresentation(BsonType.String)]
        public BookingStatus Status { get; set; }

        [BsonElement("NumberOfPersons")]
        [BsonRepresentation(BsonType.Int32)]
        public int NumberOfPersons { get; set; }
        
        [BsonElement("CreatedAt")]
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime CreatedAt { get; set; }
        
        [BsonElement("LastUpdateAt")]
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime LastUpdateAt { get; set; }
    }
}
