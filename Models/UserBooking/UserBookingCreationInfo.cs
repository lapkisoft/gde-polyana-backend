﻿using System;

namespace Models.UserBooking
{
    public class UserBookingCreationInfo
    {
        public string PlaceId { get; }
        public string PlaceName { get; set; }
        public string UserId { get; }
        public string UserPhone { get; set; }
        public DateTime OrderDateTime { get; }
        public BookingStatus Status { get; }
        public int NumberOfPersons { get; }

        public UserBookingCreationInfo(string placeId, string userId, DateTime orderDateTime, int numberOfPersons)
        {
            PlaceId = placeId ?? throw new ArgumentNullException(nameof(placeId));
            UserId = userId ?? throw new ArgumentNullException(nameof(userId));
            OrderDateTime = orderDateTime;
            Status = BookingStatus.AwaitingConfirmation;
            NumberOfPersons = numberOfPersons;
        }
    }
}
