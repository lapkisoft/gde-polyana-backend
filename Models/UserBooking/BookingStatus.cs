﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.UserBooking
{
    public enum BookingStatus
    {
        AwaitingConfirmation,
        Confirmed,
        Rejected,
        Canceled
    }
}
