﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Models.Places;
using Models.UserBooking.Exceptions;
using MongoDB.Driver;

namespace Models.UserBooking.Repositories
{
    public class MongoUserBookingRepository : IUserBookingRepository
    {
        private readonly IMongoCollection<UserBooking> userBookings;

        public MongoUserBookingRepository()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("GdePolyanaDb");
            userBookings = database.GetCollection<UserBooking>("UserBooking");
        }

        public Task<UserBooking> CreateAsync(UserBookingCreationInfo creationInfo, CancellationToken cancellationToken)
        {
            if (creationInfo == null)
            {
                throw new ArgumentNullException(nameof(creationInfo));
            }

            cancellationToken.ThrowIfCancellationRequested();
            var newId = Guid.NewGuid();
            var now = DateTime.UtcNow;

            var newUserBooking = new UserBooking
            {
                Id = newId,
                PlaceId = creationInfo.PlaceId,
                PlaceName = creationInfo.PlaceName,
                UserId = creationInfo.UserId,
                UserPhone = creationInfo.UserPhone,
                OrderDateTime = creationInfo.OrderDateTime,
                Status = creationInfo.Status,
                NumberOfPersons = creationInfo.NumberOfPersons,
                CreatedAt = now,
                LastUpdateAt = now
            };

            userBookings.InsertOne(newUserBooking, cancellationToken: cancellationToken);
            return Task.FromResult(newUserBooking);
        }

        public Task<IReadOnlyList<UserBooking>> SearchAsync(UserBookingSearchInfo searchInfo, CancellationToken cancellationToken)
        {
            if (searchInfo == null)
            {
                throw new ArgumentNullException(nameof(searchInfo));
            }
            
            cancellationToken.ThrowIfCancellationRequested();
            
            var search = userBookings.Find(item => true).ToEnumerable();

            if (searchInfo.PlaceId != null)
            {
                search = search.Where(item => searchInfo.PlaceId.Equals(item.PlaceId));
            }
            
            if (searchInfo.UserId != null)
            {
                search = search.Where(item => searchInfo.UserId.Equals(item.UserId));
            }
            
            if (searchInfo.Status != null)
            {
                search = search.Where(item => searchInfo.Status.Value == item.Status);
            }

            if (searchInfo.Offset != null)
            {
                search = search.Skip(searchInfo.Offset.Value);
            }

            if (searchInfo.Limit != null)
            {
                search = search.Take(searchInfo.Limit.Value);
            }

            search = search.OrderByDescending(item => item.CreatedAt);

            var result = search.ToList();
            return Task.FromResult<IReadOnlyList<UserBooking>>(result);
        }

        public Task<UserBooking> GetAsync(Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var userBooking = userBookings.Find(booking => booking.Id == id).FirstOrDefault();

            if (userBooking == null)
            {
                throw new UserBookingNotFoundException(id.ToString());
            }

            return Task.FromResult(userBooking);
        }


        public Task<UserBooking> PatchAsync(UserBookingPatchInfo patchInfo, CancellationToken cancellationToken)
        {
            if (patchInfo == null)
            {
                throw new ArgumentNullException(nameof(patchInfo));
            }

            cancellationToken.ThrowIfCancellationRequested();
            var userBookingInfo = userBookings.Find(booking => booking.Id == patchInfo.Id).FirstOrDefault();

            if (userBookingInfo == null)
            {
                throw new UserBookingNotFoundException(patchInfo.Id.ToString());
            }

            var updated = false;

            if (patchInfo.OrderTime != null)
            {
                userBookingInfo.OrderDateTime = patchInfo.OrderTime.Value;
                updated = true;
            }

            if (patchInfo.Status != null)
            {
                userBookingInfo.Status = (BookingStatus)patchInfo.Status;
                updated = true;
            }

            if (patchInfo.NumberOfPersons != null)
            {
                userBookingInfo.NumberOfPersons = (int)patchInfo.NumberOfPersons;
                updated = true;
            }

            if (updated)
            {
                userBookingInfo.LastUpdateAt = DateTime.UtcNow;
                userBookings.ReplaceOne(item => item.Id == patchInfo.Id, userBookingInfo);
            }

            return Task.FromResult(userBookingInfo);
        }


        public Task RemoveAsync(Guid id, CancellationToken cancellationToken)
        {
            if (id == null)
            {
                throw new ArgumentNullException(nameof(id));
            }

            cancellationToken.ThrowIfCancellationRequested();
            var deleteResult = userBookings.DeleteOne(place => place.Id == id);

            if (deleteResult.DeletedCount == 0)
            {
                throw new UserBookingNotFoundException(id.ToString());
            }

            return Task.CompletedTask;
        }
    }
}
