﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Models.UserBooking.Repositories
{
    public interface IUserBookingRepository
    {
        Task<UserBooking> CreateAsync(UserBookingCreationInfo creationInfo, CancellationToken cancellationToken);
        Task<IReadOnlyList<UserBooking>> SearchAsync(UserBookingSearchInfo searchInfo, CancellationToken cancellationToken);
        Task<UserBooking> GetAsync(Guid id, CancellationToken cancellationToken);
        Task<UserBooking> PatchAsync(UserBookingPatchInfo patchInfo, CancellationToken cancellationToken);
        Task RemoveAsync(Guid id, CancellationToken cancellationToken);
    }
}
