using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Models.PlaceCuisines.Exceptions;
using MongoDB.Driver;

namespace Models.PlaceCuisines.Repositories
{
    public class MongoPlaceCuisineRepository : IPlaceCuisineRepository
    {
        private readonly IMongoCollection<PlaceCuisine> placeCuisines;
        
        public MongoPlaceCuisineRepository()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("GdePolyanaDb");
            placeCuisines = database.GetCollection<PlaceCuisine>("PlaceCuisines");
        }
        
        public Task<PlaceCuisine> CreateAsync(PlaceCuisineCreationInfo creationInfo, CancellationToken cancellationToken)
        {
            if (creationInfo == null)
            {
                throw new ArgumentNullException(nameof(creationInfo));
            }

            cancellationToken.ThrowIfCancellationRequested();
            var placeCuisineWithSameId = placeCuisines.Find(item => item.Id == creationInfo.Id).FirstOrDefault();

            if (placeCuisineWithSameId != null)
            {
                throw new PlaceCuisineDuplicationException(creationInfo.Id);
            }

            var placeCuisine = new PlaceCuisine
            {
                Id = creationInfo.Id,
                Name = creationInfo.Name,
                Count = 0
            };

            placeCuisines.InsertOne(placeCuisine);
            return Task.FromResult(placeCuisine);
        }

        public Task<PlaceCuisine> GetAsync(string id, CancellationToken cancellationToken)
        {
            if (id == null)
            {
                throw new ArgumentNullException(nameof(id));
            }
            
            cancellationToken.ThrowIfCancellationRequested();
            var placeCuisine = placeCuisines.Find(item => item.Id == id).FirstOrDefault();
            
            if (placeCuisine == null)
            {
                throw new PlaceCuisineNotFoundException(id);
            }

            return Task.FromResult(placeCuisine);
        }

        public Task<IReadOnlyList<PlaceCuisine>> GetAllAsync(CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var cuisinesList = placeCuisines.Find(item => true).ToList();
            return Task.FromResult<IReadOnlyList<PlaceCuisine>>(cuisinesList);
        }

        public Task<PlaceCuisine> PatchAsync(PlaceCuisinePatchInfo patchInfo, CancellationToken cancellationToken)
        {
            if (patchInfo == null)
            {
                throw new ArgumentNullException(nameof(patchInfo));
            }

            cancellationToken.ThrowIfCancellationRequested();
            var placeCuisine = placeCuisines.Find(item => item.Id == patchInfo.Id).FirstOrDefault();

            if (placeCuisine == null)
            {
                throw new PlaceCuisineNotFoundException(patchInfo.Id);
            }

            if (string.IsNullOrEmpty(patchInfo.Name))
            {
                return Task.FromResult(placeCuisine);
            }

            placeCuisine.Name = patchInfo.Name;
            placeCuisines.ReplaceOne(item => item.Id == patchInfo.Id, placeCuisine);
            return Task.FromResult(placeCuisine);
        }

        public Task RemoveAsync(string id, CancellationToken cancellationToken)
        {
            if (id == null)
            {
                throw new ArgumentNullException(nameof(id));
            }

            cancellationToken.ThrowIfCancellationRequested();
            var deleteResult = placeCuisines.DeleteOne(item => item.Id == id);

            if (deleteResult.DeletedCount == 0)
            {
                throw new PlaceCuisineNotFoundException(id);
            }

            return Task.CompletedTask;
        }
        
        public Task ChangeCountersAsync(IReadOnlyList<string> ids, CounterOperations operation, CancellationToken cancellationToken)
        {
            if (ids == null)
            {
                throw new ArgumentNullException(nameof(ids));
            }

            cancellationToken.ThrowIfCancellationRequested();
            var updatedPlaceCuisines = placeCuisines.Find(item => ids.Contains(item.Id)).ToList();

            foreach (var placeCuisine in updatedPlaceCuisines)
            {
                if (operation == CounterOperations.Increment)
                {
                    placeCuisine.Count++;                    
                }
                else
                {
                    placeCuisine.Count--;
                }
                
                placeCuisines.ReplaceOne(item => item.Id == placeCuisine.Id, placeCuisine);
            }

            return Task.CompletedTask;
        }
    }
}