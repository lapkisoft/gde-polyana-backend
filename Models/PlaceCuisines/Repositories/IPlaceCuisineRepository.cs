using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Models.PlaceCuisines.Repositories
{
    public interface IPlaceCuisineRepository
    {
        Task<PlaceCuisine> CreateAsync(PlaceCuisineCreationInfo creationInfo, CancellationToken cancellationToken);
        Task<PlaceCuisine> GetAsync(string id, CancellationToken cancellationToken);
        Task<IReadOnlyList<PlaceCuisine>> GetAllAsync(CancellationToken cancellationToken);
        Task<PlaceCuisine> PatchAsync(PlaceCuisinePatchInfo patchInfo, CancellationToken cancellationToken);
        Task RemoveAsync(string id, CancellationToken cancellationToken);
        Task ChangeCountersAsync(IReadOnlyList<string> ids, CounterOperations operation, CancellationToken cancellationToken);
    }
}