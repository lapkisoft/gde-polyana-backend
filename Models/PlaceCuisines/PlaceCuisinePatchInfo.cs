namespace Models.PlaceCuisines
{
    public class PlaceCuisinePatchInfo
    {
        public string Id { get; }
        public string Name { get; set; }

        public PlaceCuisinePatchInfo(string id, string name = null)
        {
            Id = id;
            Name = name;
        }
    }
}