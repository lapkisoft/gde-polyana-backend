using System;

namespace Models.PlaceCuisines.Exceptions
{
    public class PlaceCuisineNotFoundException : Exception
    {
        /// <summary>
        /// ������� ����� ��������� ���������� � ���, ��� ��� ����� �� ������.
        /// </summary>
        /// <param name="placeCuisineId">��� ����� � ��������� ��������������� �� ������.</param>
        public PlaceCuisineNotFoundException(string placeCuisineId)
            : base($"PlaceCuisine \"{placeCuisineId}\" is not found.")
        {
        }
    }
}