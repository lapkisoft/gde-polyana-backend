using System;

namespace Models.PlaceCuisines.Exceptions
{
    public class PlaceCuisineDuplicationException : Exception
    {
        /// <summary>
        /// ������� ����� ��������� ���������� � ���, ��� ��� ����� ��� ����������.
        /// </summary>
        /// <param name="placeCuisineId">��� ����� � ��������� ��������������� ��� ����������.</param>
        public PlaceCuisineDuplicationException(string placeCuisineId)
            : base($"Place�uisine \"{placeCuisineId}\" already exists.")
        {
        }
    }
}