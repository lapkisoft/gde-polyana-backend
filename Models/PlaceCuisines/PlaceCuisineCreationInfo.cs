using System;

namespace Models.PlaceCuisines
{
    public class PlaceCuisineCreationInfo
    {
        public string Id { get; }
        public string Name { get; }

        public PlaceCuisineCreationInfo(string id, string name)
        {
            Id = id?.ToLower() ?? throw new ArgumentNullException(nameof(id));
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }
    }
}