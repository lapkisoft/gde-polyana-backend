using System;
using System.Threading.Tasks;
using AspNetCore.Identity.Mongo;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.Extensions.DependencyInjection;
using Models.PlaceCuisines.Repositories;
using Models.Places.Repositories;
using Models.PlaceTypes.Repositories;
using Models.Roles;
using Models.UserBooking.Repositories;
using Models.Users;
using Swashbuckle.AspNetCore.Swagger;

namespace API
{
    public class Startup
    {
        private const string DocsRoute = "secret-materials/docs";
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IPlaceRepository, MongoPlaceRepository>();
            services.AddSingleton<IPlaceTypeRepository, MongoPlaceTypeRepository>();
            services.AddSingleton<IPlaceCuisineRepository, MongoPlaceCuisineRepository>();
            services.AddSingleton<IUserBookingRepository, MongoUserBookingRepository>();
            services.Configure<IdentityOptions>(options =>
            {
                options.User.RequireUniqueEmail = true;
                options.Password.RequiredLength = 6;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireDigit = false;
            });
            services.ConfigureApplicationCookie(options =>
            {
                options.SlidingExpiration = true;
                options.Events.OnRedirectToLogin = context =>
                {
                    context.Response.Headers["Location"] = context.RedirectUri;
                    context.Response.StatusCode = 401;
                    return Task.CompletedTask;
                };
            });
            services.AddIdentityMongoDbProvider<User, Role>(mongo =>
                mongo.ConnectionString = "mongodb://localhost:27017/Identity");
            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory("AllowAnyPolicy"));
            });
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAnyPolicy", builder =>
                {
                    builder
                        .AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                });
            });
            services.AddMvc();

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("GP-API", new Info
                {
                    Version = "v1",
                    Title = "GdePolyana API",
                    Description = "ASP.NET Core Web API"
                });
                options.IncludeXmlComments($"{AppDomain.CurrentDomain.BaseDirectory}/API.xml");
            });
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UseStatusCodePagesWithReExecute("/index.html");

            //todo запретить cors после завершения разработки
            app.UseCors("AllowAnyPolicy");
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseMvc();

            app.UseSwagger(options => options.RouteTemplate = $"{DocsRoute}/{{documentName}}/swagger.json");
            app.UseSwaggerUI(options =>
            {
                options.RoutePrefix = DocsRoute;
                options.SwaggerEndpoint($"/{DocsRoute}/GP-API/swagger.json", "GdePolyana API");
            });
        }
    }
}