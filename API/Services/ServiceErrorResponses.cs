﻿using System;
using System.Net;
using ClientModels.Errors;

namespace API.Services
{
    public static class ServiceErrorResponses
    {
        public static ServiceErrorResponse PlaceNotFound(string placeId)
        {
            if (placeId == null)
            {
                throw new ArgumentNullException(nameof(placeId));
            }

            var error = new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.NotFound,
                Error = new ServiceError
                {
                    Code = ServiceErrorCodes.NotFound,
                    Message = $"Place with id=\"{placeId}\" not found.",
                    Target = "Place"
                }
            };

            return error;
        }

        public static ServiceErrorResponse PlaceTypeNotFound(string placeTypeId)
        {
            if (placeTypeId == null)
            {
                throw new ArgumentNullException(nameof(placeTypeId));
            }

            var error = new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.NotFound,
                Error = new ServiceError
                {
                    Code = ServiceErrorCodes.NotFound,
                    Message = $"PlaceType with id=\"{placeTypeId}\" not found.",
                    Target = "PlaceType"
                }
            };

            return error;
        }

        public static ServiceErrorResponse PlaceCuisineNotFound(string placeCuisineId)
        {
            if (placeCuisineId == null)
            {
                throw new ArgumentNullException(nameof(placeCuisineId));
            }

            var error = new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.NotFound,
                Error = new ServiceError
                {
                    Code = ServiceErrorCodes.NotFound,
                    Message = $"PlaceCuisine with id=\"{placeCuisineId}\" not found.",
                    Target = "PlaceCuisine"
                }
            };

            return error;
        }

        public static ServiceErrorResponse UserNotFound(string userId)
        {
            if (userId == null)
            {
                throw new ArgumentNullException(nameof(userId));
            }

            var error = new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.NotFound,
                Error = new ServiceError
                {
                    Code = ServiceErrorCodes.NotFound,
                    Message = $"User with id=\"{userId}\" not found.",
                    Target = "User"
                }
            };

            return error;
        }

        public static ServiceErrorResponse RoleNotFound(string roleId)
        {
            if (roleId == null)
            {
                throw new ArgumentNullException(nameof(roleId));
            }

            var error = new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.NotFound,
                Error = new ServiceError
                {
                    Code = ServiceErrorCodes.NotFound,
                    Message = $"Role with id=\"{roleId}\" not found.",
                    Target = "Role"
                }
            };

            return error;
        }

        public static ServiceErrorResponse UserBookingNotFound(string userBookingId)
        {
            if (userBookingId == null)
            {
                throw new ArgumentNullException(nameof(userBookingId));
            }

            var error = new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.NotFound,
                Error = new ServiceError
                {
                    Code = ServiceErrorCodes.NotFound,
                    Message = $"UserBooking with id=\"{userBookingId}\" not found.",
                    Target = "UserBooking"
                }
            };

            return error;
        }

        public static ServiceErrorResponse BodyIsMissing(string target)
        {
            var error = new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.BadRequest,
                Error = new ServiceError
                {
                    Code = ServiceErrorCodes.BadRequest,
                    Message = "Request body is empty.",
                    Target = target
                }
            };

            return error;
        }

        public static ServiceErrorResponse ValidationError(string target)
        {
            if (target == null)
            {
                throw new ArgumentNullException(nameof(target));
            }
            var error = new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.BadRequest,
                Error = new ServiceError
                {
                    Code = ServiceErrorCodes.ValidationError,
                    Message = $"Validation error on target",
                    Target = target
                }
            };

            return error;
        }

        public static ServiceErrorResponse PlaceAlreadyExists(string placeId)
        {
            if (placeId == null)
            {
                throw new ArgumentNullException(nameof(placeId));
            }
            var error = new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.BadRequest,
                Error = new ServiceError
                {
                    Code = ServiceErrorCodes.BadRequest,
                    Message = $"Place with id=\"{placeId}\" already exists.",
                    Target = "Place"
                }
            };

            return error;
        }

        public static ServiceErrorResponse PlaceTypeAlreadyExists(string placeTypeId)
        {
            if (placeTypeId == null)
            {
                throw new ArgumentNullException(nameof(placeTypeId));
            }
            var error = new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.BadRequest,
                Error = new ServiceError
                {
                    Code = ServiceErrorCodes.BadRequest,
                    Message = $"PlaceType with id=\"{placeTypeId}\" already exists.",
                    Target = "PlaceType"
                }
            };

            return error;
        }

        public static ServiceErrorResponse PlaceCuisineAlreadyExists(string placeCuisineId)
        {
            if (placeCuisineId == null)
            {
                throw new ArgumentNullException(nameof(placeCuisineId));
            }
            var error = new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.BadRequest,
                Error = new ServiceError
                {
                    Code = ServiceErrorCodes.BadRequest,
                    Message = $"PlaceCuisine with id=\"{placeCuisineId}\" already exists.",
                    Target = "PlaceCuisine"
                }
            };

            return error;
        }

        public static ServiceErrorResponse UserNameAlreadyUse(string userName)
        {
            if (userName == null)
            {
                throw new ArgumentNullException(nameof(userName));
            }
            var error = new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.BadRequest,
                Error = new ServiceError
                {
                    Code = ServiceErrorCodes.BadRequest,
                    Message = $"The userName \"{userName}\" is already in use.",
                    Target = "UserName"
                }
            };

            return error;
        }

        public static ServiceErrorResponse InvalidImageData(string target)
        {
            var error = new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.BadRequest,
                Error = new ServiceError
                {
                    Code = ServiceErrorCodes.BadRequest,
                    Message = "Avatar image is invalid.",
                    Target = target
                }
            };

            return error;
        }

        public static ServiceErrorResponse AuthIsNotConfirmed(string target)
        {
            var error = new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.Unauthorized,
                Error = new ServiceError
                {
                    Code = ServiceErrorCodes.Forbidden,
                    Message = "Authorization forbidden.",
                    Target = target
                }
            };

            return error;
        }
    }
}