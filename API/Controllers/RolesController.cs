﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using API.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Models.Roles;
using Models.Users;
using Model = Models.Roles;
using Client = ClientModels.Roles;
using Microsoft.AspNetCore.Authorization;

namespace API.Controllers
{
    /// <summary>
    /// Контроллер ролей
    /// </summary>
    [Route("api/v1/roles")]
    public class RolesController : ControllerBase
    {
        private readonly RoleManager<Role> roleManager;
        private readonly UserManager<User> userManager;

        /// <summary>
        /// Конструктор ролей
        /// </summary>
        /// <param name="roleManager">Менеджер ролей</param>
        /// <param name="userManager">Менеджер пользователей</param>
        public RolesController(RoleManager<Role> roleManager, UserManager<User> userManager)
        {
            this.roleManager = roleManager ?? throw new ArgumentNullException(nameof(roleManager));
            this.userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        /// <summary>
        /// Получение списка ролей
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> GetAllRoles(CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var roles = await Task.Run((() => roleManager.Roles));

            IReadOnlyList<Client.Role> clientRoles;
            try
            {
                clientRoles = roles.Select(item => ModelConverters.Roles.RoleConverter.Convert(item)).ToImmutableList();
            }
            catch (ArgumentNullException)
            {
                var error = ServiceErrorResponses.RoleNotFound("roles");
                return NotFound(error);
            }

            return Ok(clientRoles);
        }

        /// <summary>
        /// Получение модели роли по названию
        /// </summary>
        /// <param name="name">Название роли</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{name}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> GetRole([FromRoute] string name, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var role = await roleManager.FindByNameAsync(name);
            try
            {
                var clientRoles = ModelConverters.Roles.RoleConverter.Convert(role);
            }
            catch (ArgumentNullException)
            {
                var error = ServiceErrorResponses.RoleNotFound("role");
                return NotFound(error);
            }

            return Ok(role);
        }


        /// <summary>
        /// Изменение роли пользователя
        /// </summary>
        /// <param name="userName">Логин пользователя</param>
        /// <param name="clientPatchInfo">Модель изменения пользователя</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpPatch]
        [Route("{userName}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> PatchRoleAsync([FromRoute]string userName, [FromBody] Client.RoleUserPatchInfo clientPatchInfo,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (clientPatchInfo == null)
            {
                var error = ServiceErrorResponses.BodyIsMissing(nameof(clientPatchInfo));
                return BadRequest(error);
            }

            RoleUserPatchInfo modelPatchInfo;
            try
            {
                modelPatchInfo = ModelConverters.Roles.RoleUserPatchInfoConverter.Convert(userName, clientPatchInfo);
            }
            catch (ArgumentNullException ex)
            {
                var error = ServiceErrorResponses.BodyIsMissing(ex.Message);
                return BadRequest(error);
            }

            var user = await userManager.FindByNameAsync(userName);
            if (user == null)
            {
                var error = ServiceErrorResponses.RoleNotFound("role");
                return BadRequest(error);
            }

            var modelRole = await roleManager.FindByNameAsync(modelPatchInfo.UserRole.ToLower());

            if (modelRole == null)
            {
                var error = ServiceErrorResponses.RoleNotFound(modelPatchInfo.UserRole);
                return BadRequest(error);
            }

            if (user.Roles.Contains(modelPatchInfo.UserRole.ToUpper()))
            {
                await userManager.RemoveFromRoleAsync(user, modelPatchInfo.UserRole);
            }
            else
            {
                await userManager.AddToRoleAsync(user, modelPatchInfo.UserRole);
            }

            return Ok(user);
        }
        
    }
}
