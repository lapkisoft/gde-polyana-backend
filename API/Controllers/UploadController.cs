using System.IO;
using System.Threading;
using System.Threading.Tasks;
using API.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models.Places.Repositories;
using Model = Models.Places;

namespace API.Controllers
{
    [Route("api/v1/upload")]
    public sealed class UploadController : ControllerBase
    {
        private readonly IHostingEnvironment hostingEnvironment;
        private readonly IPlaceRepository placeRepository;

        public UploadController(IHostingEnvironment hostingEnvironment, IPlaceRepository placeRepository)
        {
            this.hostingEnvironment = hostingEnvironment;
            this.placeRepository = placeRepository;
        }
        
        /// <summary>
        /// Загружает аватарку заведения
        /// </summary>
        /// <param name="id">Идентификатор заведения</param>
        /// <param name="avatar">Картинка аватарки</param>
        /// <param name="cancellationToken"></param>
        /// <response code="200">Созданная аватарка</response>
        /// <response code="400">Если аватарку забыли указать, или она оказалась некорректной</response>
        [HttpPost]
        [Route("")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> UploadAvatarAsync(string id, IFormFile avatar, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            
            if (id == null)
            {
                var error = ServiceErrorResponses.BodyIsMissing(nameof(id));
                return BadRequest(error);
            }
            
            if (avatar == null)
            {
                var error = ServiceErrorResponses.BodyIsMissing(nameof(avatar));
                return BadRequest(error);
            }

            const int maxFileLength = 1024 * 512;
            var extension = Path.GetExtension(avatar.FileName);
            var path = $"/avatars/{id}{extension}";
            var stream = avatar.OpenReadStream();

            if (avatar.Length > 0 && avatar.Length <= maxFileLength && ImageValidationService.IsImage(stream))
            {
                using (var fileStream = new FileStream($"{hostingEnvironment.WebRootPath}{path}", FileMode.Create))
                {
                    await avatar.CopyToAsync(fileStream, cancellationToken).ConfigureAwait(false);
                }
                
                var placePatchInfo = new Model.PlacePatchInfo(id, null, path);
                await placeRepository.PatchAsync(placePatchInfo, cancellationToken).ConfigureAwait(false);
            }
            else
            {
                var error = ServiceErrorResponses.InvalidImageData(nameof(avatar));
                return BadRequest(error);
            }

            var clientAvatar = new ClientModels.Avatar(path);
            return Ok(clientAvatar);
        }
    }
}