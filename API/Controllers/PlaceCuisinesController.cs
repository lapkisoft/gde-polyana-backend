using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Models.PlaceCuisines.Repositories;
using Model = Models.PlaceCuisines;
using Client = ClientModels.PlaceCuisines;
using Converter = ModelConverters.PlaceCuisines;
using API.Services;
using Models.PlaceCuisines.Exceptions;
using System.Linq;
using System.Collections.Immutable;

namespace API.Controllers
{
    [Route("api/v1/place-cuisines")]
    public class PlaceCuisinesController : ControllerBase
    {
        private readonly IPlaceCuisineRepository repository;

        public PlaceCuisinesController(IPlaceCuisineRepository repository)
        {
            this.repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }
        
        /// <summary>
        /// Создаёт тип кухни заведения
        /// </summary>
        /// <param name="creationInfo">Информация о создаваемом типе кухни заведения</param>
        /// <param name="cancellationToken"></param>
        [HttpPost]
        [Route("")]
        public async Task<IActionResult> CreatePlaceCuisineAsync([FromBody]Client.PlaceCuisineCreationInfo creationInfo,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (creationInfo == null)
            {
                var error = ServiceErrorResponses.BodyIsMissing(nameof(creationInfo));
                return BadRequest(error);
            }

            if (!ModelState.IsValid)
            {
                var errorTarget = ModelState.Values
                    .First()
                    .Errors
                    .First()
                    .ErrorMessage;

                var error = ServiceErrorResponses.ValidationError(errorTarget);
                return BadRequest(error);
            }

            var modelCreationInfo = Converter.PlaceCuisineCreationInfoConverter.Convert(creationInfo);
            Model.PlaceCuisine modelPlaceCuisine;

            try
            {
                modelPlaceCuisine =
                    await repository.CreateAsync(modelCreationInfo, cancellationToken).ConfigureAwait(false);
            }
            catch (PlaceCuisineDuplicationException)
            {
                var error = ServiceErrorResponses.PlaceCuisineAlreadyExists(creationInfo.Id);
                return BadRequest(error);
            }

            var clientPlaceCuisine = Converter.PlaceCuisineConverter.Convert(modelPlaceCuisine);
            return CreatedAtRoute("GetPlaceCuisineRoute", new { id = clientPlaceCuisine.Id }, clientPlaceCuisine);
        }

        /// <summary>
        /// Возвращает список всех типов кухонь заведений
        /// </summary>
        /// <param name="cancellationToken"></param>
        [HttpGet]
        [Route("")]
        public async Task<IActionResult> GetPlaceCuisinesAsync(CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var modelPlaceCuisinesList = await repository.GetAllAsync(cancellationToken).ConfigureAwait(false);
            var clientPlaceCuisinesList = modelPlaceCuisinesList.Select(Converter.PlaceCuisineConverter.Convert).ToImmutableList();

            return Ok(clientPlaceCuisinesList);
        }

        /// <summary>
        /// Возвращает тип кухни заведения
        /// </summary>
        /// <param name="id">Идентификатор типа кухни заведения</param>
        /// <param name="cancellationToken"></param>
        [HttpGet]
        [Route("{id}", Name = "GetPlaceCuisineRoute")]
        public async Task<IActionResult> GetPlaceCuisineAsync([FromRoute]string id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            Models.PlaceCuisines.PlaceCuisine modelPlaceCuisine;

            try
            {
                modelPlaceCuisine = await repository.GetAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (PlaceCuisineNotFoundException)
            {
                var error = ServiceErrorResponses.PlaceCuisineNotFound(id);
                return NotFound(error);
            }

            var clientPlaceCuisine = Converter.PlaceCuisineConverter.Convert(modelPlaceCuisine);
            return Ok(clientPlaceCuisine);
        }

        /// <summary>
        /// Изменяет тип кухни заведения
        /// </summary>
        /// <param name="id">Идентификатор типа кухни заведения</param>
        /// <param name="patchInfo">Новые значения параметров для типа кухни заведения</param>
        /// <param name="cancellationToken"></param>
        [HttpPatch]
        [Route("{id}")]
        public async Task<IActionResult> PatchPlaceCuisineAsync([FromRoute] string id,
            [FromBody] Client.PlaceCuisinePatchInfo patchInfo, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (patchInfo == null)
            {
                var error = ServiceErrorResponses.BodyIsMissing(nameof(patchInfo));
                return BadRequest(error);
            }

            var modelPatchInfo = Converter.PlaceCuisinePatchInfoConverter.Convert(id, patchInfo);
            Models.PlaceCuisines.PlaceCuisine modelPlaceCuisine;

            try
            {
                modelPlaceCuisine = await repository.PatchAsync(modelPatchInfo, cancellationToken).ConfigureAwait(false);
            }
            catch (PlaceCuisineNotFoundException)
            {
                var error = ServiceErrorResponses.PlaceCuisineNotFound(id);
                return NotFound(error);
            }

            var clientPlaceCuisine = ModelConverters.PlaceCuisines.PlaceCuisineConverter.Convert(modelPlaceCuisine);
            return Ok(clientPlaceCuisine);
        }

        /// <summary>
        /// Удаляет тип кухни заведения
        /// </summary>
        /// <param name="id">Идентификатор типа кухни заведения</param>
        /// <param name="cancellationToken"></param>
        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> DeletePlaceCuisineAsync([FromRoute]string id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            try
            {
                await repository.RemoveAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (PlaceCuisineNotFoundException)
            {
                var error = ServiceErrorResponses.PlaceCuisineNotFound(id);
                return NotFound(error);
            }

            return NoContent();
        }
    }
}