using System;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Models.PlaceTypes.Exceptions;
using Models.PlaceTypes.Repositories;
using Model = Models.PlaceTypes;
using Client = ClientModels.PlaceTypes;
using Converter = ModelConverters.PlaceTypes;
using API.Services;

namespace API.Controllers
{
    [Route("api/v1/place-types")]
    public sealed class PlaceTypesController : ControllerBase
    {
        private readonly IPlaceTypeRepository repository;

        public PlaceTypesController(IPlaceTypeRepository repository)
        {
            this.repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        /// <summary>
        /// Создаёт тип заведения
        /// </summary>
        /// <param name="creationInfo">Информация о создаваемом типе заведения</param>
        /// <param name="cancellationToken"></param>
        [HttpPost]
        [Route("")]
        public async Task<IActionResult> CreatePlaceTypeAsync([FromBody]Client.PlaceTypeCreationInfo creationInfo,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (creationInfo == null)
            {
                var error = ServiceErrorResponses.BodyIsMissing(nameof(creationInfo));
                return BadRequest(error);
            }

            if (!ModelState.IsValid)
            {
                var errorTarget = ModelState.Values
                    .First()
                    .Errors
                    .First()
                    .ErrorMessage;

                var error = ServiceErrorResponses.ValidationError(errorTarget);
                return BadRequest(error);
            }

            var modelCreationInfo = Converter.PlaceTypeCreationInfoConverter.Convert(creationInfo);
            Model.PlaceType modelPlaceType;

            try
            {
                modelPlaceType =
                    await repository.CreateAsync(modelCreationInfo, cancellationToken).ConfigureAwait(false);
            }
            catch (PlaceTypeDuplicationException)
            {
                var error = ServiceErrorResponses.PlaceTypeAlreadyExists(creationInfo.Id);
                return BadRequest(error);
            }

            var clientPlaceType = Converter.PlaceTypeConverter.Convert(modelPlaceType);
            return CreatedAtRoute("GetPlaceTypeRoute", new { id = clientPlaceType.Id }, clientPlaceType);
        }

        /// <summary>
        /// Возвращает список всех типов заведений
        /// </summary>
        /// <param name="cancellationToken"></param>
        [HttpGet]
        [Route("")]
        public async Task<IActionResult> GetPlaceTypesAsync(CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var modelPlaceTypesList = await repository.GetAllAsync(cancellationToken).ConfigureAwait(false);
            var clientPlaceTypesList = modelPlaceTypesList.Select(Converter.PlaceTypeConverter.Convert).ToImmutableList();

            return Ok(clientPlaceTypesList);
        }

        /// <summary>
        /// Возвращает тип заведения
        /// </summary>
        /// <param name="id">Идентификатор типа заведения</param>
        /// <param name="cancellationToken"></param>
        [HttpGet]
        [Route("{id}", Name = "GetPlaceTypeRoute")]
        public async Task<IActionResult> GetPlaceTypeAsync([FromRoute]string id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            Models.PlaceTypes.PlaceType modelPlaceType;

            try
            {
                modelPlaceType = await repository.GetAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (PlaceTypeNotFoundException)
            {
                var error = ServiceErrorResponses.PlaceTypeNotFound(id);
                return NotFound(error);
            }

            var clientPlaceType = Converter.PlaceTypeConverter.Convert(modelPlaceType);
            return Ok(clientPlaceType);
        }

        /// <summary>
        /// Изменяет тип заведения
        /// </summary>
        /// <param name="id">Идентификатор типа заведения</param>
        /// <param name="patchInfo">Новые значения параметров для типа заведения</param>
        /// <param name="cancellationToken"></param>
        [HttpPatch]
        [Route("{id}")]
        public async Task<IActionResult> PatchPlaceTypeAsync([FromRoute] string id,
            [FromBody] Client.PlaceTypePatchInfo patchInfo, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (patchInfo == null)
            {
                var error = ServiceErrorResponses.BodyIsMissing(nameof(patchInfo));
                return BadRequest(error);
            }

            var modelPatchInfo = Converter.PlaceTypePatchInfoConverter.Convert(id, patchInfo);
            Models.PlaceTypes.PlaceType modelPlaceType;

            try
            {
                modelPlaceType = await repository.PatchAsync(modelPatchInfo, cancellationToken).ConfigureAwait(false);
            }
            catch (PlaceTypeNotFoundException)
            {
                var error = ServiceErrorResponses.PlaceTypeNotFound(id);
                return NotFound(error);
            }

            var clientPlaceType = ModelConverters.PlaceTypes.PlaceTypeConverter.Convert(modelPlaceType);
            return Ok(clientPlaceType);
        }

        /// <summary>
        /// Удаляет тип заведения
        /// </summary>
        /// <param name="id">Идентификатор типа заведения</param>
        /// <param name="cancellationToken"></param>
        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> DeletePlaceTypeAsync([FromRoute]string id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            try
            {
                await repository.RemoveAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (PlaceTypeNotFoundException)
            {
                var error = ServiceErrorResponses.PlaceTypeNotFound(id);
                return NotFound(error);
            }

            return NoContent();
        }
    }
}