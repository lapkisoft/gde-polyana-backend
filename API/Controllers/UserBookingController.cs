﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Models.UserBooking.Repositories;
using Model = Models.UserBooking;
using Client = ClientModels.UserBooking;
using System.Threading;
using System.Threading.Tasks;
using API.Services;
using Converter = ModelConverters.UserBooking;
using Models.UserBooking.Exceptions;
using System.Linq;
using System.Collections.Immutable;
using System.Runtime.CompilerServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Models.Places;
using Models.Users;
using Models.Places.Repositories;
using Models.Places.Exceptions;
using MongoDB.Driver.Core.Operations;

namespace API.Controllers
{
    [Route("api/v1/booking")]
    public class UserBookingController : ControllerBase
    {
        private readonly IUserBookingRepository bookingRepository;
        private readonly IPlaceRepository placeRepository;
        private readonly UserManager<User> userManager;

        public UserBookingController(IUserBookingRepository bookingRepository, IPlaceRepository placeRepository, UserManager<User> userManager)
        {
            this.bookingRepository = bookingRepository ?? throw new ArgumentNullException(nameof(bookingRepository));
            this.placeRepository = placeRepository ?? throw new ArgumentNullException(nameof(placeRepository));
            this.userManager = userManager ?? throw new ArgumentException(nameof(userManager));
        }

        /// <summary>
        /// Создаёт информацию о бронировании
        /// Достпно только авторизованным пользователям и администратору
        /// </summary>
        /// <param name="creationInfo">Информация о создаваемом бронировании</param>
        /// <param name="cancellationToken"></param>
        [HttpPost]
        [Authorize(Roles = "admin, user")]
        [Route("")]
        public async Task<IActionResult> CreateUserBookingAsync([FromBody]Client.UserBookingCreationInfo creationInfo,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (creationInfo == null)
            {
                var error = ServiceErrorResponses.BodyIsMissing(nameof(creationInfo));
                return BadRequest(error);
            }

            var userName = HttpContext.User.Identity.Name;
            var user = await userManager.FindByNameAsync(userName);

            if (user == null)
            {
                var error = ServiceErrorResponses.UserNotFound(userName);
                return BadRequest(error);
            }

            string placeName;

            Place place;

            try
            {
                place = await placeRepository.GetAsync(creationInfo.PlaceId, cancellationToken).ConfigureAwait(false);
                placeName = place.Name;
            }
            catch (PlaceNotFoundException)
            {
                var error = ServiceErrorResponses.PlaceNotFound(creationInfo.PlaceId);
                return BadRequest(error);
            }

            var modelCreationInfo = Converter.UserBookingCreationInfoConverter.Convert(creationInfo, user.Id);
            modelCreationInfo.PlaceName = placeName;
            modelCreationInfo.UserPhone = user.PhoneNumber;

            var isOrderTimeValid = CompareWorkTime(place, modelCreationInfo.OrderDateTime);

            if (!isOrderTimeValid)
            {
                var error = ServiceErrorResponses.ValidationError(modelCreationInfo.OrderDateTime.ToString());
                return BadRequest(error);
            }

            var modelUserBooking = await bookingRepository.CreateAsync(modelCreationInfo, cancellationToken).ConfigureAwait(false);
            var clientUserBooking = Converter.UserBookingConverter.Convert(modelUserBooking);
            return CreatedAtRoute("GetUserBookingRoute", new { id = clientUserBooking.Id }, clientUserBooking);
        }


        private static bool CompareWorkTime(Place place, DateTime orderDate)
        {
            var NskTimeZone = new TimeSpan(07, 00, 00);
            var timeAtNsk = DateTime.UtcNow.Add(NskTimeZone);

            if (orderDate.CompareTo(timeAtNsk) <= 0)
            {
                return false;
            }

            var tempDay = (int)orderDate.DayOfWeek;
            var currentDayOfWeek = tempDay - 1 < 0 ? 6 : tempDay - 1;
            var previousDayOfWeek = currentDayOfWeek - 1 < 0 ? 6 : currentDayOfWeek - 1;

            if (place.WorkingTime[currentDayOfWeek].OpenTime != place.WorkingTime[currentDayOfWeek].CloseTime)
            {
                var workTimeToday = place.WorkingTime[currentDayOfWeek];
                var workTimeYesterday = place.WorkingTime[previousDayOfWeek];
                var orderTime = orderDate.TimeOfDay;

                if (orderTime < workTimeToday.OpenTime && (workTimeToday.OpenTime < workTimeYesterday.CloseTime
                    || workTimeToday.OpenTime > workTimeYesterday.CloseTime && orderTime >= workTimeYesterday.CloseTime)
                    || orderTime >= workTimeToday.OpenTime && workTimeToday.OpenTime < workTimeToday.CloseTime && orderTime >= workTimeToday.CloseTime)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Возвращает список бронирований
        /// </summary>
        /// <param name="searchInfo">Параметры запроса</param>
        /// <param name="cancellationToken"></param>
        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<IActionResult> SearchUserBookingAsync([FromQuery]Client.UserBookingSearchInfo searchInfo,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (!HttpContext.User.IsInRole("admin"))
            {
                var userName = HttpContext?.User?.Identity?.Name;
                if (userName == null)
                {
                    return Forbid();
                }

                var user = await userManager.FindByNameAsync(userName);

                if (user == null)
                {
                    var error = ServiceErrorResponses.UserNotFound(userName);
                    return BadRequest(error);
                }

                searchInfo.UserId = user.Id;
            }

            var modelSearchInfo = Converter.UserBookingSearchInfoConverter.Convert(searchInfo ?? new Client.UserBookingSearchInfo());
            var modelUserBookingList = await bookingRepository.SearchAsync(modelSearchInfo, cancellationToken).ConfigureAwait(false);
            var clientUserBookingList = modelUserBookingList.Select(Converter.UserBookingConverter.Convert).ToImmutableList();

            return Ok(clientUserBookingList);
        }

        /// <summary>
        /// Возвращает информацию о бронировании
        /// Доступно только создателю брони и администратору
        /// </summary>
        /// <param name="id">Идентификатор брони</param>
        /// <param name="cancellationToken"></param>
        [HttpGet]
        [Authorize]
        [Route("{id}", Name = "GetUserBookingRoute")]
        public async Task<IActionResult> GetUserBookingAsync([FromRoute]Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (id == Guid.Empty)
            {
                var error = ServiceErrorResponses.ValidationError(nameof(id));
                return BadRequest(error);
            }

            var userName = HttpContext.User.Identity.Name;
            var user = await userManager.FindByNameAsync(userName);

            if (user == null)
            {
                var error = ServiceErrorResponses.ValidationError(nameof(user));
                return BadRequest(error);
            }

            Model.UserBooking modelUserBooking;

            try
            {
                modelUserBooking = await bookingRepository.GetAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (UserBookingNotFoundException)
            {
                var error = ServiceErrorResponses.UserBookingNotFound(id.ToString());
                return NotFound(error);
            }

            if (!(modelUserBooking.UserId.Equals(user.Id) || HttpContext.User.IsInRole("admin")))
            {
                return Forbid();
            }

            var clientUserBooking = Converter.UserBookingConverter.Convert(modelUserBooking);
            return Ok(clientUserBooking);
        }

        /// <summary>
        /// Изменяет информацию бронирования
        /// Доступно пользователю создавшему бронь и админу
        /// Статус может изменить только админ, но пользователь также может отменять свою бронь
        /// </summary>
        /// <param name="id">Идентификатор бронирования</param>
        /// <param name="patchInfo">Новые значения параметров бронирования</param>
        /// <param name="cancellationToken"></param>
        [HttpPatch]
        [Authorize]
        [Route("{id}")]
        public async Task<IActionResult> PatchUserBookingAsync([FromRoute]Guid id,
            [FromBody] Client.UserBookingPatchInfo patchInfo, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (id == Guid.Empty)
            {
                var error = ServiceErrorResponses.ValidationError(nameof(id));
                return BadRequest(error);
            }

            if (patchInfo == null)
            {
                var error = ServiceErrorResponses.BodyIsMissing(nameof(patchInfo));
                return BadRequest(error);
            }

            var userName = HttpContext.User.Identity.Name;
            var user = await userManager.FindByNameAsync(userName);

            if (user == null)
            {
                var error = ServiceErrorResponses.ValidationError("user");
                return BadRequest(error);
            }

            try
            {
                var modelBooking = await bookingRepository.GetAsync(id, cancellationToken).ConfigureAwait(false);

                if (!(modelBooking.UserId.Equals(user.Id) || HttpContext.User.IsInRole("admin")))
                {
                    return Forbid();
                }
            }
            catch (UserBookingNotFoundException)
            {
                var error = ServiceErrorResponses.UserBookingNotFound(id.ToString());
                return NotFound(error);
            }

            var modelPatchInfo = Converter.UserBookingPatchInfoConverter.Convert(id, patchInfo);

            if (!HttpContext.User.IsInRole("admin") && modelPatchInfo.Status != Model.BookingStatus.Canceled)
            {
                modelPatchInfo.Status = null;
            }

            Model.UserBooking modelUserBooking;
            Place place;

            try
            {
                modelUserBooking = await bookingRepository.GetAsync(modelPatchInfo.Id, cancellationToken).ConfigureAwait(false);
            }
            catch (UserBookingNotFoundException)
            {
                var error = ServiceErrorResponses.UserBookingNotFound(id.ToString());
                return NotFound(error);
            }

            try
            {
                place = await placeRepository.GetAsync(modelUserBooking.PlaceId, cancellationToken).ConfigureAwait(false);
            }
            catch (PlaceNotFoundException)
            {
                var error = ServiceErrorResponses.PlaceNotFound(modelUserBooking.PlaceId);
                return BadRequest(error);
            }

            if (modelPatchInfo.OrderTime != null)
            {
                var isOrderTimeValid = CompareWorkTime(place, (DateTime)modelPatchInfo.OrderTime);

                if (!isOrderTimeValid)
                {
                    var error = ServiceErrorResponses.ValidationError(modelPatchInfo.OrderTime.ToString());
                    return BadRequest(error);
                }
            }

            try
            {
                modelUserBooking = await bookingRepository.PatchAsync(modelPatchInfo, cancellationToken).ConfigureAwait(false);
            }
            catch (UserBookingNotFoundException)
            {
                var error = ServiceErrorResponses.UserBookingNotFound(id.ToString());
                return NotFound(error);
            }

            var clientUserBooking = ModelConverters.UserBooking.UserBookingConverter.Convert(modelUserBooking);
            return Ok(clientUserBooking);
        }

        /// <summary>
        /// Удаляет бронирование
        /// Только админ
        /// </summary>
        /// <param name="id">Идентификатор бронирования</param>
        /// <param name="cancellationToken"></param>
        [HttpDelete]
        [Authorize(Roles = "admin")]
        [Route("{id}")]
        public async Task<IActionResult> DeleteUserBookingAsync([FromRoute]Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (id == Guid.Empty)
            {
                var error = ServiceErrorResponses.ValidationError(nameof(id));
                return BadRequest(error);
            }

            try
            {
                await bookingRepository.RemoveAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (UserBookingNotFoundException)
            {
                var error = ServiceErrorResponses.UserBookingNotFound(id.ToString());
                return NotFound(error);
            }

            return NoContent();
        }
    }
}
