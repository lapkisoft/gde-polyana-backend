﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using API.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Models.UserIdentity;
using Models.Users;

namespace API.Controllers
{
    /// <summary>
    /// Контроллер аутентификации
    /// </summary>
    [Route("api/v1")]
    public class AccountController : ControllerBase
    {
        private readonly UserManager<User> userManager;
        private readonly SignInManager<User> signInManager;

        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager)
        {
            this.userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
            this.signInManager = signInManager ?? throw new ArgumentNullException(nameof(signInManager));
        }

        /// <summary>
        /// Аутентификация пользователя
        /// </summary>
        /// <param name="clientUserLogin">Пользовательские данные для входа</param>
        /// <param name="cancellationToken"></param>
        [HttpPost]
        [AllowAnonymous]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] ClientModels.UserIdentity.UserLogin clientUserLogin, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (clientUserLogin == null)
            {
                var error = ServiceErrorResponses.BodyIsMissing(nameof(clientUserLogin));
                return BadRequest(error);
            }

            var modelUserLogin = ModelConverters.UserIdentity.UserLoginConverter.Convert(clientUserLogin);
            var result = await signInManager.PasswordSignInAsync(modelUserLogin.Username, modelUserLogin.Password, modelUserLogin.RememberMe, false);
            
            if (!result.Succeeded)
            {
                var error = ServiceErrorResponses.ValidationError(nameof(clientUserLogin));
                return BadRequest(error);
            }

            return Ok(result);
        }

        /// <summary>
        /// Выход из текущей сессии
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("logout")]
        public async Task<IActionResult> Logout()
        {
            await signInManager.SignOutAsync();
            return Ok();
        }
    }
}
