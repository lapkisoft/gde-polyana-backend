﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using API.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Models.Users;
using Model = Models.Users;
using Client = ClientModels.Users;
using Models.UserBooking.Repositories;
using Models.Places.Repositories;
using Models.Places.Exceptions;
using Models.UserBooking.Exceptions;
using Models.Roles;

namespace API.Controllers
{
    /// <summary>
    /// Контролллер пользователей
    /// </summary>
    [Route("api/v1/users")]
    public class UsersController : ControllerBase
    {
        private readonly UserManager<User> userManager;
        private readonly IPlaceRepository placeRepository;
        private readonly IUserBookingRepository userBookingRepository;
        private readonly RoleManager<Role> roleManager;

        public UsersController(UserManager<User> userManager, IPlaceRepository placeRepository,
            IUserBookingRepository userBookingRepository, RoleManager<Role> roleManager)
        {
            this.userManager = userManager ?? throw new ArgumentException(nameof(userManager));
            this.placeRepository = placeRepository ?? throw new ArgumentException(nameof(placeRepository));
            this.userBookingRepository = userBookingRepository ?? throw new ArgumentException(nameof(userBookingRepository));
            this.roleManager = roleManager ?? throw new ArgumentNullException(nameof(roleManager));
        }

        /// <summary>
        /// Создание пользователя
        /// </summary>
        /// <param name="clientUserInfo">Модель создания </param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("")]
        public async Task<IActionResult> CreateAsync([FromBody] Client.UserCreationInfo clientUserInfo,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            //todo проверку на успешность создания
            if (!ModelState.IsValid)
            {
                var errorTarget = ModelState.Values
                    .First()
                    .Errors
                    .First()
                    .ErrorMessage;

                var error = ServiceErrorResponses.ValidationError(errorTarget);
                return BadRequest(error);
            }

            if (clientUserInfo == null)
            {
                var error = ServiceErrorResponses.BodyIsMissing(nameof(clientUserInfo));
                return BadRequest(error);
            }

            var modelCreationInfo = ModelConverters.Users.UserCreationInfoConverter.Convert(clientUserInfo);
            var user = await userManager.FindByNameAsync(modelCreationInfo.UserName);

            if (user != null)
            {
                var error = ServiceErrorResponses.UserNameAlreadyUse(clientUserInfo.UserName);
                return BadRequest(error);
            }

            var dateTime = DateTime.UtcNow;
            var modelUser = new User
            {
                UserName = modelCreationInfo.UserName,
                Email = modelCreationInfo.Email,
                PhoneNumber = modelCreationInfo.PhoneNumber,
                IsManagerRoleRequested = false,
                RegisteredAt = dateTime,
                LastUpdatedAt = dateTime
            };

            var result = await userManager.CreateAsync(modelUser, modelCreationInfo.Password);

            if (!result.Succeeded)
            {
                var error = ServiceErrorResponses.ValidationError(result.Errors.First().ToString());
                return BadRequest(error);
            }

            await userManager.AddToRoleAsync(modelUser, "user");
            var clientUser = ModelConverters.Users.UserConverter.Convert(modelUser);
            return CreatedAtRoute("GetUserRoute", new { username = clientUser.UserName }, clientUser);
        }

        /// <summary>
        /// Получение списка пользователей
        /// </summary>
        /// <param name="searchInfo"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> GetAllUsers([FromQuery]Client.UserSearchInfo searchInfo, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var users = await Task.Run((() => userManager.Users));

            var modelSearchInfo =
                ModelConverters.Users.UserSearchInfoConverter.Convert(searchInfo ?? new Client.UserSearchInfo());

            if (searchInfo.IsManagerRoleRequested != null)
            {
                users = users.Where(item => searchInfo.IsManagerRoleRequested.Value == item.IsManagerRoleRequested);
            }

            if (searchInfo.Offset != null)
            {
                users = users.Skip(searchInfo.Offset.Value);
            }

            if (searchInfo.Limit != null)
            {
                users = users.Take(searchInfo.Limit.Value);
            }

            users = users.OrderByDescending(item => item.LastUpdatedAt);

            try
            {
                IReadOnlyList<Client.User> clientRoles = users.Select(item => ModelConverters.Users.UserConverter.Convert(item))
                    .ToImmutableList();
            }
            catch (ArgumentNullException ex)
            {
                var error = ServiceErrorResponses.UserNotFound(ex.Message);
                return NotFound(error);
            }

            var clientUsers = users.Select(user => ModelConverters.Users.UserConverter.Convert(user));
            return Ok(clientUsers);
        }

        /// <summary>
        /// Получение пользователя по логину
        /// </summary>
        /// <param name="userName">Логин пользователя</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{userName}", Name = "GetUserRoute")]
        [Authorize]
        public async Task<IActionResult> GetUser([FromRoute]string userName, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var isAuthorize = HttpContext.User.IsInRole("admin") ||
                              HttpContext.User.Identity.Name.CompareTo(userName.ToLower()) == 0;

            if (!isAuthorize)
            {
                return Forbid();
            }

            var user = await userManager.FindByNameAsync(userName);

            if (user == null)
            {
                var error = ServiceErrorResponses.UserNotFound(userName);
                return BadRequest(error);
            }

            var clientUser = ModelConverters.Users.UserConverter.Convert(user);
            return Ok(clientUser);
        }

        /// <summary>
        /// Получение информации о пользователе по cookie
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("userInfo")]
        [Authorize]
        public async Task<IActionResult> GetUserInfo(CancellationToken cancellationToken)
        {
            if (HttpContext.User != null &&
                HttpContext.User.Identity != null &&
                HttpContext.User.Identity.IsAuthenticated)
            {
                var user = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);

                if (user != null)
                {
                    var clientUser = ModelConverters.Users.UserConverter.Convert(user);
                    return Ok(clientUser);
                }
            }

            var error = ServiceErrorResponses.ValidationError("user");
            return NotFound(error);
        }

        /// <summary>
        /// Модификация пользовательских данных
        /// </summary>
        /// <param name="username">Логин пользователя</param>
        /// <param name="clientPatchInfo">Модель модификации пользователя</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpPatch]
        [Route("{username}")]
        [Authorize]
        public async Task<IActionResult> PatchUserAsync([FromRoute] string username, [FromBody] Client.UserPatchInfo clientPatchInfo,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (clientPatchInfo == null)
            {
                var error = ServiceErrorResponses.BodyIsMissing(nameof(clientPatchInfo));
                return BadRequest(error);
            }

            var isAuthorize = HttpContext.User.IsInRole("admin") ||
                              HttpContext.User.Identity.Name.CompareTo(username.ToLower()) == 0;

            if (!isAuthorize)
            {
                return Forbid();
            }

            var modelPatchInfo = ModelConverters.Users.UserPatchInfoConverter.Convert(username, clientPatchInfo);

            var user = await userManager.FindByNameAsync(username);

            if (user == null)
            {
                var error = ServiceErrorResponses.UserNotFound(username);
                return BadRequest(error);
            }

            var updated = false;

            if (modelPatchInfo.Password != null && modelPatchInfo.OldPassword != null)
            {
                var result =
                    await userManager.ChangePasswordAsync(user, modelPatchInfo.OldPassword, modelPatchInfo.Password);

                if (result.Succeeded)
                {
                    updated = true;
                }
                else
                {
                    var error = ServiceErrorResponses.ValidationError("Password");
                    return BadRequest(error);
                }
            }

            if (modelPatchInfo.IsManagerRoleRequested != null)
            {
                user.IsManagerRoleRequested = (bool)modelPatchInfo.IsManagerRoleRequested;
                updated = true;
            }

            if (updated)
            {
                user.LastUpdatedAt = DateTime.UtcNow;
                await userManager.UpdateAsync(user);
            }

            var clientUser = ModelConverters.Users.UserConverter.Convert(user);

            return Ok(clientUser);
        }


        /// <summary>
        /// Удаление пользователя
        /// </summary>
        /// <param name="username">Логин пользователя</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("{username}")]
        public async Task<ActionResult> Delete([FromRoute]string username, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var isAuthorize = HttpContext.User.IsInRole("admin") ||
                              HttpContext.User.Identity.Name.CompareTo(username.ToLower()) == 0;

            if (!isAuthorize)
            {
                return Forbid();
            }

            var user = await userManager.FindByNameAsync(username);
            if (user == null)
            {
                var error = ServiceErrorResponses.UserNotFound(username);
                return NotFound(error);
            }

            var result = await userManager.DeleteAsync(user);
            return NoContent();
        }
    }
}
