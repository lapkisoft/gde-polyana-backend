using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Models.Places.Exceptions;
using Models.Places.Repositories;
using Models.PlaceTypes.Repositories;
using Model = Models.Places;
using Client = ClientModels.Places;
using Converter = ModelConverters.Places;
using API.Services;
using Microsoft.AspNetCore.Authorization;
using Models;
using Models.PlaceCuisines.Repositories;
using Microsoft.AspNetCore.Identity;
using Models.Users;
using Models.Roles;

namespace API.Controllers
{
    [Route("api/v1/places")]
    public sealed class PlacesController : ControllerBase
    {
        private readonly IPlaceRepository placeRepository;
        private readonly IPlaceTypeRepository placeTypeRepository;
        private readonly IPlaceCuisineRepository placeCuisineRepository;
        private readonly UserManager<User> userManager;
        private readonly RoleManager<Role> roleManager;

        public PlacesController(IPlaceRepository placeRepository,
                        IPlaceTypeRepository placeTypeRepository,
                        IPlaceCuisineRepository placeCuisineRepository,
                        UserManager<User> userManager, 
                        RoleManager<Role> roleManager)
        {
            this.placeRepository = placeRepository ?? throw new ArgumentNullException(nameof(placeRepository));
            this.placeTypeRepository = placeTypeRepository ?? throw new ArgumentNullException(nameof(placeTypeRepository));
            this.placeCuisineRepository = placeCuisineRepository ?? throw new ArgumentNullException(nameof(placeCuisineRepository));
            this.userManager = userManager ?? throw new ArgumentException(nameof(userManager));
            this.roleManager = roleManager ?? throw new ArgumentNullException(nameof(roleManager));
        }

        /// <summary>
        /// Создаёт новое заведение
        /// Доступно только администратору и пользователю в роли менеджер
        /// </summary>
        /// <param name="creationInfo">Информация о создаваемом заведении</param>
        /// <param name="cancellationToken"></param>
        /// <response code="201">Возвращает созданный объект</response>
        /// <response code="400">Если creationInfo == null или PlaceDuplicationException</response> 
        [HttpPost]
        [Authorize (Roles = "manager, admin")]
        [Route("")]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> CreatePlaceAsync([FromBody]Client.PlaceCreationInfo creationInfo,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (!ModelState.IsValid)
            {
                var errorTarget = ModelState.Values
                    .First()
                    .Errors
                    .First()
                    .ErrorMessage;

                var error = ServiceErrorResponses.ValidationError(errorTarget);
                return BadRequest(error);
            }

            if (creationInfo == null)
            {
                var error = ServiceErrorResponses.BodyIsMissing(nameof(creationInfo));
                return BadRequest(error);
            }

            var userName = HttpContext.User.Identity.Name;

            var user = await userManager.FindByNameAsync(userName);
            if (user == null)
            {
                var error = ServiceErrorResponses.UserNotFound(userName);
                return BadRequest(error);
            }

            var modelTypes = await placeTypeRepository.GetAllAsync(cancellationToken).ConfigureAwait(false);
            var modelCuisines = await placeCuisineRepository.GetAllAsync(cancellationToken).ConfigureAwait(false);
            Model.PlaceCreationInfo modelCreationInfo;
            try
            {
                modelCreationInfo =
                    Converter.PlaceCreationInfoConverter.Convert(creationInfo, user.Id, modelTypes, modelCuisines);
            }

            catch (ArgumentNullException ex)
            {
                return BadRequest(ex);
            }

            catch (ArgumentException ex)
            {
                var error = ServiceErrorResponses.ValidationError(ex.Message);
                return BadRequest(error);
            }

            Model.Place modelPlace;

            try
            {
                modelPlace = await placeRepository.CreateAsync(modelCreationInfo, cancellationToken)
                    .ConfigureAwait(false);
            }
            catch (PlaceDuplicationException)
            {
                var error = ServiceErrorResponses.PlaceAlreadyExists(creationInfo.Id);
                return BadRequest(error);
            }

            await placeTypeRepository
                .ChangeCountersAsync(modelCreationInfo.Types, CounterOperations.Increment, cancellationToken)
                .ConfigureAwait(false);
            await placeCuisineRepository
                .ChangeCountersAsync(modelCreationInfo.Cuisines, CounterOperations.Increment, cancellationToken)
                .ConfigureAwait(false);

            var clientPlace = Converter.PlaceConverter.Convert(modelPlace, modelTypes, modelCuisines);
            return CreatedAtRoute("GetPlaceRoute", new { id = clientPlace.Id }, clientPlace);
        }

        /// <summary>
        /// Выполняет выборку заведений
        /// Доступно всем пользователям
        /// </summary>
        /// <param name="searchInfo">Параметры запроса</param>
        /// <param name="cancellationToken"></param>
        [HttpGet]
        [AllowAnonymous]
        [Route("")]
        public async Task<IActionResult> SearchPlacesAsync([FromQuery]Client.PlaceSearchInfo searchInfo,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var modelSearchInfo = Converter.PlaceSearchInfoConverter.Convert(searchInfo ?? new Client.PlaceSearchInfo());
            var modelPlaceList = await placeRepository.SearchAsync(modelSearchInfo, cancellationToken).ConfigureAwait(false);
            var modelTypes = await placeTypeRepository.GetAllAsync(cancellationToken).ConfigureAwait(false);
            var modelCuisines = await placeCuisineRepository.GetAllAsync(cancellationToken).ConfigureAwait(false);
            var clientPlaceList = modelPlaceList.Select(place => Converter.PlaceConverter.Convert(place, modelTypes, modelCuisines));
            clientPlaceList = await HideTelegramTokens(clientPlaceList);

            return Ok(clientPlaceList);
        }

        /// <summary>
        /// Возвращает информацию о заведении
        /// Доступно всем пользователям
        /// </summary>
        /// <param name="id">Идентификатор заведения</param>
        /// <param name="cancellationToken"></param>
        [HttpGet]
        [AllowAnonymous]
        [Route("{id}", Name = "GetPlaceRoute")]
        public async Task<IActionResult> GetPlaceAsync([FromRoute]string id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            Models.Places.Place modelPlace;

            try
            {
                modelPlace = await placeRepository.GetAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (PlaceNotFoundException)
            {
                var error = ServiceErrorResponses.PlaceNotFound(id);
                return NotFound(error);
            }

            var modelTypes = await placeTypeRepository.GetAllAsync(cancellationToken).ConfigureAwait(false);
            var modelCuisines = await placeCuisineRepository.GetAllAsync(cancellationToken).ConfigureAwait(false);
            var clientPlace = Converter.PlaceConverter.Convert(modelPlace, modelTypes, modelCuisines);
            clientPlace = await HideTelegramTokens(clientPlace);
            
            return Ok(clientPlace);
        }
        
        /// <summary>
        /// Возвращает информацию о заведении по токену телеграма
        /// Доступно менеджеру, админу
        /// </summary>
        /// <param name="telegramToken">Telegram token</param>
        /// <param name="cancellationToken"></param>
        [HttpGet]
        [Authorize(Roles = "manager, admin")]
        [Route("telegram/{telegramToken}")]
        public async Task<IActionResult> GetPlaceByTelegramTokenAsync([FromRoute]Guid telegramToken, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (telegramToken == Guid.Empty)
            {
                var error = ServiceErrorResponses.ValidationError(nameof(telegramToken));
                return BadRequest(error);
            }
            
            Models.Places.Place modelPlace;

            try
            {
                modelPlace = await placeRepository.GetByTokenAsync(telegramToken, cancellationToken).ConfigureAwait(false);
            }
            catch (PlaceNotFoundException)
            {
                var error = ServiceErrorResponses.PlaceNotFound(string.Empty);
                return NotFound(error);
            }

            var modelTypes = await placeTypeRepository.GetAllAsync(cancellationToken).ConfigureAwait(false);
            var modelCuisines = await placeCuisineRepository.GetAllAsync(cancellationToken).ConfigureAwait(false);
            var clientPlace = Converter.PlaceConverter.Convert(modelPlace, modelTypes, modelCuisines);
            return Ok(clientPlace);
        }

        /// <summary>
        /// Изменяет информацию о заведении
        /// Доступно только администратору и пользователю в роли менеджер создавшему заведение
        /// </summary>
        /// <param name="id">Идентификатор заведения</param>
        /// <param name="patchInfo">Новые значения параметров заведения</param>
        /// <param name="cancellationToken"></param>
        [HttpPatch]
        [Authorize(Roles = "manager, admin")]
        [Route("{id}")]
        public async Task<IActionResult> PatchPlaceAsync([FromRoute] string id,
            [FromBody] Client.PlacePatchInfo patchInfo, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (patchInfo == null)
            {
                var error = ServiceErrorResponses.BodyIsMissing(nameof(patchInfo));
                return BadRequest(error);
            }

            var userName = HttpContext.User.Identity.Name;
            var user = await userManager.FindByNameAsync(userName);

            if (user == null)
            {
                var error = ServiceErrorResponses.ValidationError("user");
                return BadRequest(error);
            }

            var modelTypes = await placeTypeRepository.GetAllAsync(cancellationToken).ConfigureAwait(false);
            var modelCuisines = await placeCuisineRepository.GetAllAsync(cancellationToken).ConfigureAwait(false);
            var modelPatchInfo = Converter.PlacePatchInfoConverter.Convert(id, patchInfo, modelTypes, modelCuisines);
            Model.Place modelPlace;

            try
            {
                if (modelPatchInfo.Types != null || modelPatchInfo.Cuisines != null)
                {
                    modelPlace = await placeRepository.GetAsync(modelPatchInfo.Id, cancellationToken)
                        .ConfigureAwait(false);
                    
                    if (!(modelPlace.Manager.Equals(user.Id) || HttpContext.User.IsInRole("admin")))
                    {
                        return Forbid();
                    }

                    if (modelPatchInfo.Types != null && modelPatchInfo.Types.Any())
                    {
                        await placeTypeRepository
                            .ChangeCountersAsync(modelPlace.Types, CounterOperations.Decrement, cancellationToken)
                            .ConfigureAwait(false);
                    }

                    if (modelPatchInfo.Cuisines != null)
                    {
                        await placeCuisineRepository
                            .ChangeCountersAsync(modelPlace.Cuisines, CounterOperations.Decrement, cancellationToken)
                            .ConfigureAwait(false);
                    }
                }

                modelPlace = await placeRepository.PatchAsync(modelPatchInfo, cancellationToken).ConfigureAwait(false);
            }
            catch (PlaceNotFoundException)
            {
                var error = ServiceErrorResponses.PlaceNotFound(id);
                return NotFound(error);
            }

            if (modelPatchInfo.Types != null && modelPatchInfo.Types.Any())
            {
                await placeTypeRepository
                    .ChangeCountersAsync(modelPlace.Types, CounterOperations.Increment, cancellationToken)
                    .ConfigureAwait(false);
            }

            if (modelPatchInfo.Cuisines != null)
            {
                await placeCuisineRepository
                    .ChangeCountersAsync(modelPatchInfo.Cuisines, CounterOperations.Increment, cancellationToken)
                    .ConfigureAwait(false);
            }

            var clientPlace = Converter.PlaceConverter.Convert(modelPlace, modelTypes, modelCuisines);
            return Ok(clientPlace);
        }

        /// <summary>
        /// Удаляет заведение
        /// Доступно только администратору и пользователю в роли менеджер создавшему заведение
        /// </summary>
        /// <param name="id">Идентификатор заведения</param>
        /// <param name="cancellationToken"></param>
        [HttpDelete]
        [Authorize(Roles = "admin, manager")]
        [Route("{id}")]
        public async Task<IActionResult> DeletePlaceAsync([FromRoute]string id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var userName = HttpContext.User.Identity.Name;
            var user = await userManager.FindByNameAsync(userName);

            if (user == null)
            {
                var error = ServiceErrorResponses.ValidationError("user");
                return BadRequest(error);
            }

            try
            {
                var modelPlace = await placeRepository.GetAsync(id, cancellationToken).ConfigureAwait(false);
                
                if (!(modelPlace.Manager.Equals(user.Id) || HttpContext.User.IsInRole("admin")))
                {
                    return Forbid();
                }
                
                await placeTypeRepository
                    .ChangeCountersAsync(modelPlace.Types, CounterOperations.Decrement, cancellationToken)
                    .ConfigureAwait(false);
                await placeCuisineRepository
                    .ChangeCountersAsync(modelPlace.Cuisines, CounterOperations.Decrement, cancellationToken)
                    .ConfigureAwait(false);
                await placeRepository.RemoveAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (PlaceNotFoundException)
            {
                var error = ServiceErrorResponses.PlaceNotFound(id);
                return NotFound(error);
            }

            return NoContent();
        }

        private async Task<IEnumerable<Client.Place>> HideTelegramTokens(IEnumerable<Client.Place> clientPlaces)
        {
            var userName = HttpContext.User.Identity.Name;
            User user = null;

            if (userName != null)
            {
                user = await userManager.FindByNameAsync(userName);
            }

            if (user == null || !HttpContext.User.IsInRole("manager") && !HttpContext.User.IsInRole("admin"))
            {
                clientPlaces = clientPlaces.Select(item =>
                {
                    item.TelegramToken = Guid.Empty.ToString();
                    return item;
                });
            }
            else
            {
                var isAdmin = HttpContext.User.IsInRole("admin");
                
                clientPlaces = clientPlaces.Select(item =>
                {
                    if (isAdmin || item.Manager.Equals(user.Id))
                    {
                        return item;
                    }

                    item.TelegramToken = Guid.Empty.ToString();
                    return item;
                });
            }

            return clientPlaces;
        }
        
        private async Task<Client.Place> HideTelegramTokens(Client.Place clientPlace)
        {
            var userName = HttpContext.User.Identity.Name;
            User user = null;

            if (userName != null)
            {
                user = await userManager.FindByNameAsync(userName);
            }

            if (user == null || !(HttpContext.User.IsInRole("manager") && user.Id.Equals(clientPlace.Manager)) && !HttpContext.User.IsInRole("admin"))
            {
                clientPlace.TelegramToken = Guid.Empty.ToString();
            }
            
            return clientPlace;
        }
    }
}