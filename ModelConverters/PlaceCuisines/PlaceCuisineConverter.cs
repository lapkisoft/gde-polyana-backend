using System;
using Model = Models.PlaceCuisines;
using Client = ClientModels.PlaceCuisines;

namespace ModelConverters.PlaceCuisines
{
    public static class PlaceCuisineConverter
    {
        public static Client.PlaceCuisine Convert(Model.PlaceCuisine modelPlaceCuisine)
        {
            if (modelPlaceCuisine == null)
            {
                throw new ArgumentNullException(nameof(modelPlaceCuisine));
            }

            var clientPlaceCuisine = new Client.PlaceCuisine
            {
                Id = modelPlaceCuisine.Id, 
                Name = modelPlaceCuisine.Name, 
                Count = modelPlaceCuisine.Count
            };
            
            return clientPlaceCuisine;
        }
    }
}