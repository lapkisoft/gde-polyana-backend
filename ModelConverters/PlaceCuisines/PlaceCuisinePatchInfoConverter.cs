using System;
using Model = Models.PlaceCuisines;
using Client = ClientModels.PlaceCuisines;

namespace ModelConverters.PlaceCuisines
{
    public class PlaceCuisinePatchInfoConverter
    {
        public static Model.PlaceCuisinePatchInfo Convert(string id, Client.PlaceCuisinePatchInfo clientPatchInfo)
        {
            if (clientPatchInfo == null)
            {
                throw new ArgumentNullException(nameof(clientPatchInfo));
            }

            var modelPatchInfo = new Model.PlaceCuisinePatchInfo(id, clientPatchInfo.Name);
            return modelPatchInfo;
        }
    }
}