using System;
using Model = Models.PlaceCuisines;
using Client = ClientModels.PlaceCuisines;

namespace ModelConverters.PlaceCuisines
{
    public static class PlaceCuisineCreationInfoConverter
    {
        public static Model.PlaceCuisineCreationInfo Convert(Client.PlaceCuisineCreationInfo clientCreationInfo)
        {
            if (clientCreationInfo == null)
            {
                throw new ArgumentNullException(nameof(clientCreationInfo));
            }

            var modelCreationInfo = new Model.PlaceCuisineCreationInfo(clientCreationInfo.Id, clientCreationInfo.Name);
            return modelCreationInfo;
        }
    }
}