﻿using System;
using Model = Models.PlaceTypes;
using Client = ClientModels.PlaceTypes;

namespace ModelConverters.PlaceTypes
{
    public static class PlaceTypePatchInfoConverter
    {
        public static Model.PlaceTypePatchInfo Convert(string id, Client.PlaceTypePatchInfo clientPatchInfo)
        {
            if (clientPatchInfo == null)
            {
                throw new ArgumentNullException(nameof(clientPatchInfo));
            }

            var modelPatchInfo = new Model.PlaceTypePatchInfo(id, clientPatchInfo.Name);
            return modelPatchInfo;
        }
    }
}
