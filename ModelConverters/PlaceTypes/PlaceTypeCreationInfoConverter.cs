using System;
using Model = Models.PlaceTypes;
using Client = ClientModels.PlaceTypes;

namespace ModelConverters.PlaceTypes
{
    public static class PlaceTypeCreationInfoConverter
    {
        public static Model.PlaceTypeCreationInfo Convert(Client.PlaceTypeCreationInfo clientCreationInfo)
        {
            if (clientCreationInfo == null)
            {
                throw new ArgumentNullException(nameof(clientCreationInfo));
            }

            var modelCreationInfo = new Model.PlaceTypeCreationInfo(clientCreationInfo.Id, clientCreationInfo.Name);
            return modelCreationInfo;
        }
    }
}