using System;
using Model = Models.PlaceTypes;
using Client = ClientModels.PlaceTypes;

namespace ModelConverters.PlaceTypes
{
    public static class PlaceTypeConverter
    {
        public static Client.PlaceType Convert(Model.PlaceType modelPlaceType)
        {
            if (modelPlaceType == null)
            {
                throw new ArgumentNullException(nameof(modelPlaceType));
            }

            var clientPlaceType = new Client.PlaceType
            {
                Id = modelPlaceType.Id, 
                Name = modelPlaceType.Name, 
                Count = modelPlaceType.Count
            };
            
            return clientPlaceType;
        }
    }
}