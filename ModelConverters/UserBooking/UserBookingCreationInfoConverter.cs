﻿using System;
using Model = Models.UserBooking;
using Client = ClientModels.UserBooking;

namespace ModelConverters.UserBooking
{
    public static class UserBookingCreationInfoConverter
    {
        public static Model.UserBookingCreationInfo Convert(Client.UserBookingCreationInfo clientCreationInfo, string userId)
        {
            if (clientCreationInfo == null)
            {
                throw new ArgumentNullException(nameof(clientCreationInfo));
            }

            if (userId == null)
            {
                throw new ArgumentNullException(nameof(userId));
            }

            var modelDateTime =
                UserBookingConverterUtils.ConvertDateTimeFromClientToModel(clientCreationInfo.OrderDate,
                    clientCreationInfo.OrderTime);

            var modelCreationInfo = new Model.UserBookingCreationInfo(
                clientCreationInfo.PlaceId,
                userId,
                modelDateTime,
                clientCreationInfo.NumberOfPersons);

            return modelCreationInfo;
        }
    }
}