﻿using System;
using System.Linq;
using Model = Models.UserBooking;

namespace ModelConverters.UserBooking
{
    public class UserBookingConverterUtils
    {
        public static Model.BookingStatus? BookingStatusConverter(string status)
        {
            Model.BookingStatus? modelBookingStatus = null;
            //ToDo Ограничить величину при вводе численного значения
            if (Enum.TryParse(status, true, out Model.BookingStatus tmpBookingStatus))
            {
                modelBookingStatus = tmpBookingStatus;
            }

            return modelBookingStatus;
        }

        public static DateTime ConvertDateTimeFromClientToModel(string date, string time)
        {
            if (date == null)
            {
                throw new ArgumentNullException(nameof(date));
            }

            if (time == null)
            {
                throw new ArgumentNullException(nameof(time));
            }

            var dateElements = date.Split('-')
                .Select(int.Parse)
                .ToArray();
            var timeElements = time.Split(':')
                .Select(int.Parse)
                .ToArray();
            var modelDateTime = 
                new DateTime(dateElements[0], dateElements[1], dateElements[2], timeElements[0], timeElements[1], 0);
            var normalizedDateTime = DateTime.SpecifyKind(modelDateTime, DateTimeKind.Utc);

            return normalizedDateTime;
        }

        public static string ConvertDateFromModelToClient(DateTime dateTime)
        {
            var clientDate = $"{dateTime.Year}-{dateTime.Month}-{dateTime.Day}";
            return clientDate;
        }

        public static string ConvertTimeFromModelToClient(DateTime dateTime)
        {
            var hourString = dateTime.Hour < 10 ? $"0{dateTime.Hour}" : $"{dateTime.Hour}";
            var minuteString = dateTime.Minute < 10 ? $"0{dateTime.Minute}" : $"{dateTime.Minute}";
            var clientTime = $"{hourString}:{minuteString}";
            return clientTime;
        }
    }
}
