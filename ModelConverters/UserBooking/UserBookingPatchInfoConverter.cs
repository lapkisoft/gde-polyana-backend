﻿using System;
using Model = Models.UserBooking;
using Client = ClientModels.UserBooking;

namespace ModelConverters.UserBooking
{
    public static class UserBookingPatchInfoConverter
    {
        public static Model.UserBookingPatchInfo Convert(Guid id, Client.UserBookingPatchInfo clientPatchInfo)
        {
            if (clientPatchInfo == null)
            {
                throw new ArgumentNullException(nameof(clientPatchInfo));
            }

            var userBookingStatus = UserBookingConverterUtils.BookingStatusConverter(clientPatchInfo.Status);
            DateTime? modelDateTime = null;
            
            if (clientPatchInfo.OrderDate != null && clientPatchInfo.OrderTime != null)
            {
                modelDateTime =
                    UserBookingConverterUtils.ConvertDateTimeFromClientToModel(clientPatchInfo.OrderDate,
                        clientPatchInfo.OrderTime);                
            }
            

            var modelPatchInfo = new Model.UserBookingPatchInfo(id)
            {
                OrderTime = modelDateTime,
                Status = userBookingStatus,
                NumberOfPersons = clientPatchInfo.NumberOfPersons
            };

            return modelPatchInfo;
        }
    }
}
