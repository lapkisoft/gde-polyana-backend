using System;
using Model = Models.UserBooking;
using Client = ClientModels.UserBooking;

namespace ModelConverters.UserBooking
{
    public static class UserBookingSearchInfoConverter
    {
        public static Model.UserBookingSearchInfo Convert(Client.UserBookingSearchInfo clientSearchInfo)
        {
            if (clientSearchInfo == null)
            {
                throw new ArgumentNullException(nameof(clientSearchInfo));
            }

            Model.BookingStatus? bookingStatus = null;
            
            if (clientSearchInfo.Status != null)
            {
                bookingStatus = UserBookingConverterUtils.BookingStatusConverter(clientSearchInfo.Status);   
            }
            
            var modelSearchInfo = new Model.UserBookingSearchInfo
            {
                Offset = clientSearchInfo.Offset,
                Limit = clientSearchInfo.Limit,
                Status = bookingStatus,
                UserId = clientSearchInfo.UserId,
                PlaceId = clientSearchInfo.PlaceId
            };

            return modelSearchInfo;
        }
    }
}