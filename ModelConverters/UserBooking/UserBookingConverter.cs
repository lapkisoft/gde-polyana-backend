﻿using System;
using System.Collections.Generic;
using System.Text;
using Client = ClientModels.UserBooking;
using Model = Models.UserBooking;

namespace ModelConverters.UserBooking
{
    public static class UserBookingConverter
    {
        public static Client.UserBooking Convert(Model.UserBooking modelUserBooking)
        {
            if (modelUserBooking == null)
            {
                throw new ArgumentNullException(nameof(modelUserBooking));
            }

            var clientOrderDate = UserBookingConverterUtils.ConvertDateFromModelToClient(modelUserBooking.OrderDateTime);
            var clientOrderTime = UserBookingConverterUtils.ConvertTimeFromModelToClient(modelUserBooking.OrderDateTime);

            var clientUserBooking = new Client.UserBooking
            {
                Id = modelUserBooking.Id.ToString(),
                PlaceId = modelUserBooking.PlaceId,
                PlaceName = modelUserBooking.PlaceName,
                UserId = modelUserBooking.UserId.ToString(),
                UserPhone = modelUserBooking.UserPhone,
                OrderDate = clientOrderDate,
                OrderTime = clientOrderTime,
                Status = modelUserBooking.Status.ToString(),
                NumberOfPersons = modelUserBooking.NumberOfPersons,
                CreatedAt = modelUserBooking.CreatedAt,
                LastUpdateAt = modelUserBooking.LastUpdateAt
            };

            return clientUserBooking;
        }
    }
}
