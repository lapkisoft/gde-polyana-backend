using System;
using System.Collections.Generic;
using System.Linq;
using Model = Models.Places;
using Client = ClientModels.Places;

namespace ModelConverters.Places
{
    public static class PlaceConverter
    {
        public static Client.Place Convert(Model.Place modelPlace,
            IReadOnlyList<Models.PlaceTypes.PlaceType> modelTypes, 
            IReadOnlyList<Models.PlaceCuisines.PlaceCuisine> modelCuisines)
        {
            if (modelPlace == null)
            {
                throw new ArgumentNullException(nameof(modelPlace));
            }

            if (modelTypes == null)
            {
                throw new ArgumentNullException(nameof(modelTypes));
            }
            
            if (modelCuisines == null)
            {
                throw new ArgumentNullException(nameof(modelCuisines));
            }

            var clientTypes = PlaceConverterUtils.ModelToClientTypeIdsConverter(modelPlace.Types, modelTypes);
            var clientCuisines = PlaceConverterUtils.ModelToClientCuisineIdsConverter(modelPlace.Cuisines, modelCuisines);
            var clientWorkingTime = PlaceConverterUtils.ConvertWorkingTimeFromModelToClient(modelPlace.WorkingTime).ToArray();

            var clientPlace = new Client.Place
            {
                Id = modelPlace.Id,
                Name = modelPlace.Name,
                Avatar = modelPlace.Avatar,
                Description = modelPlace.Description,
                Address = modelPlace.Address,
                Coordinates = modelPlace.Coordinates,
                Phone = modelPlace.Phone,
                Types = clientTypes,
                Cuisines = clientCuisines,
                AvgCheck = modelPlace.AvgCheck.ToString(),
                WorkingTime = clientWorkingTime,
                Manager = modelPlace.Manager,
                TelegramToken = modelPlace.TelegramToken.ToString(),
                CreatedAt = modelPlace.CreatedAt,
                LastUpdateAt = modelPlace.LastUpdateAt
            };

            return clientPlace;
        }
    }
}