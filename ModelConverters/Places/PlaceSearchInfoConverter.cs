using System;
using System.Linq;
using Model = Models.Places;
using Client = ClientModels.Places;

namespace ModelConverters.Places
{
    public static class PlaceSearchInfoConverter
    {
        public static Model.PlaceSearchInfo Convert(Client.PlaceSearchInfo clientSearchInfo)
        {
            if (clientSearchInfo == null)
            {
                throw new ArgumentNullException(nameof(clientSearchInfo));
            }

            Model.PlaceAvgCheck?[] avgChecks = null;
            
            if (clientSearchInfo.AvgCheck != null)
            {
                avgChecks = clientSearchInfo.AvgCheck.Select(PlaceConverterUtils.AvgCheckConverter).ToArray();   
            }
            
            var modelSearchInfo = new Model.PlaceSearchInfo
            {
                Offset = clientSearchInfo.Offset,
                Limit = clientSearchInfo.Limit,
                AvgChecks = avgChecks,
                Types = clientSearchInfo.Type,
                Cuisines = clientSearchInfo.Cuisine,
                Manager = clientSearchInfo.Manager
            };

            return modelSearchInfo;
        }
    }
}