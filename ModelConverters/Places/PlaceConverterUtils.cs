using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Model = Models.Places;
using Client = ClientModels.Places;

namespace ModelConverters.Places
{
    public static class PlaceConverterUtils
    {
        public static Model.PlaceAvgCheck? AvgCheckConverter(string avgCheck)
        {
            Model.PlaceAvgCheck? modelAvgCheck = null;

            if (Enum.TryParse(avgCheck, true, out Model.PlaceAvgCheck tmpAvgCheck))
            {
                modelAvgCheck = tmpAvgCheck;
            }

            return modelAvgCheck;
        }
        
        public static IEnumerable<string> TypeIdsConverter(IReadOnlyList<string> clientTypes, 
            IReadOnlyList<Models.PlaceTypes.PlaceType> modelTypes)
        {
            if (clientTypes == null)
            {
                throw new ArgumentNullException(nameof(clientTypes));
            }

            if (modelTypes == null)
            {
                throw new ArgumentNullException(nameof(modelTypes));
            }

            var modelTypeIds = clientTypes.Where(item => modelTypes.Any(type => type.Id == item));
            return modelTypeIds;
        }

        public static IEnumerable<string> CuisineIdsConverter(IReadOnlyList<string> clientCuisines,
            IReadOnlyList<Models.PlaceCuisines.PlaceCuisine> modelCuisines)
        {
            if (clientCuisines == null)
            {
                throw new ArgumentNullException(nameof(clientCuisines));
            }

            if (modelCuisines == null)
            {
                throw new ArgumentNullException(nameof(modelCuisines));
            }

            var modelCuisineIds = clientCuisines.Where(item => modelCuisines.Any(cuisine => cuisine.Id == item));
            return modelCuisineIds;
        }
        
        public static List<string> ModelToClientTypeIdsConverter(IReadOnlyList<string> placeTypes, 
            IReadOnlyList<Models.PlaceTypes.PlaceType> modelTypes)
        {
            if (placeTypes == null)
            {
                throw new ArgumentNullException(nameof(placeTypes));
            }
            
            if (modelTypes == null)
            {
                throw new ArgumentNullException(nameof(modelTypes));
            }

            var clientTypes = placeTypes
                .Select(item => modelTypes.FirstOrDefault(type => type.Id == item)?.Name)
                .ToList();
            return clientTypes;
        }

        public static List<string> ModelToClientCuisineIdsConverter(IReadOnlyList<string> placeCuisines, 
            IReadOnlyList<Models.PlaceCuisines.PlaceCuisine> modelCuisines)
        {
            if (placeCuisines == null)
            {
                throw new ArgumentNullException(nameof(placeCuisines));
            }
            
            if (modelCuisines == null)
            {
                throw new ArgumentNullException(nameof(modelCuisines));
            }

            var clientCuisines = placeCuisines
                .Select(item => modelCuisines.FirstOrDefault(cuisine => cuisine.Id == item)?.Name)
                .ToList();
            return clientCuisines;
        }

        public static IEnumerable<Model.PlaceWorkingTime> ConvertWorkingTimeFromClientToModel(
            IReadOnlyList<Client.PlaceWorkingTime> clientWorkingTime)
        {
            if (clientWorkingTime == null)
            {
                throw new ArgumentNullException(nameof(clientWorkingTime));
            }

            var modelWorkingTime = clientWorkingTime.Select(item =>
                {
                    if (!int.TryParse(item.OpenHourTime, out var openHourValue) || !IsHourValueCorrect(openHourValue))
                    {
                        throw new InvalidDataException(GetInvalidDataMessage(nameof(item.OpenHourTime)));
                    }
                    
                    if (!int.TryParse(item.OpenMinuteTime, out var openMinuteValue) || !IsMinuteValueCorrect(openMinuteValue))
                    {
                        throw new InvalidDataException(GetInvalidDataMessage(nameof(item.OpenMinuteTime)));
                    }
                    
                    if (!int.TryParse(item.CloseHourTime, out var closeHourValue) || !IsHourValueCorrect(closeHourValue))
                    {
                        throw new InvalidDataException(GetInvalidDataMessage(nameof(item.CloseHourTime)));
                    }
                    
                    if (!int.TryParse(item.CloseMinuteTime, out var closeMinuteValue) || !IsMinuteValueCorrect(closeMinuteValue))
                    {
                        throw new InvalidDataException(GetInvalidDataMessage(nameof(item.CloseMinuteTime)));
                    }
                    
                    return new Model.PlaceWorkingTime
                    {
                        OpenTime = new TimeSpan(openHourValue, openMinuteValue, 0),
                        CloseTime = new TimeSpan(closeHourValue, closeMinuteValue, 0)
                    };
                });

            return modelWorkingTime;
        }

        public static IEnumerable<Client.PlaceWorkingTime> ConvertWorkingTimeFromModelToClient(
            IEnumerable<Model.PlaceWorkingTime> modelWorkingTime)
        {
            if (modelWorkingTime == null)
            {
                throw new ArgumentNullException(nameof(modelWorkingTime));
            }

            var clientWorkingTime = modelWorkingTime.Select(item => new Client.PlaceWorkingTime
            {
                OpenHourTime = item.OpenTime.Hours < 10 ? $"0{item.OpenTime.Hours}" : item.OpenTime.Hours.ToString(),
                OpenMinuteTime = item.OpenTime.Minutes < 10 ? $"0{item.OpenTime.Minutes}" : item.OpenTime.Minutes.ToString(),
                CloseHourTime = item.CloseTime.Hours < 10 ? $"0{item.CloseTime.Hours}" : item.CloseTime.Hours.ToString(),
                CloseMinuteTime = item.CloseTime.Minutes < 10 ? $"0{item.CloseTime.Minutes}" : item.CloseTime.Minutes.ToString()
            });

            return clientWorkingTime;
        }

        private static string GetInvalidDataMessage(string dataName)
        {
            return $"{dataName} data is invalid.";
        }

        private static bool IsHourValueCorrect(int hourValue)
        {
            return hourValue >= 0 && hourValue <= 23;
        }

        private static bool IsMinuteValueCorrect(int minuteValue)
        {
            return minuteValue >= 0 && minuteValue <= 59;
        }
    }
}