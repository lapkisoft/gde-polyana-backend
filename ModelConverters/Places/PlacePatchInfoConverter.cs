﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Model = Models.Places;
using Client = ClientModels.Places;

namespace ModelConverters.Places
{
    public static class PlacePatchInfoConverter
    {
        public static Model.PlacePatchInfo Convert(string id, Client.PlacePatchInfo clientPatchInfo,
            IReadOnlyList<Models.PlaceTypes.PlaceType> modelTypes, 
            IReadOnlyList<Models.PlaceCuisines.PlaceCuisine> modelCuisines)
        {
            if (clientPatchInfo == null)
            {
                throw new ArgumentNullException(nameof(clientPatchInfo));
            }

            if (clientPatchInfo.Coordinates != null && clientPatchInfo.Coordinates.Count != 2)
            {
                throw new InvalidDataException(
                    $"{nameof(clientPatchInfo.Coordinates)} must contain 2 values (long and lat).");
            }
            
            IEnumerable<string> modelTypesIds = null, modelCuisinesIds = null;
            
            if (clientPatchInfo.Types != null)
            {
                if (modelTypes == null)
                {
                    throw new ArgumentNullException(nameof(modelTypes));
                }
                
                modelTypesIds = PlaceConverterUtils.TypeIdsConverter(clientPatchInfo.Types, modelTypes);
            }
            
            if (clientPatchInfo.Cuisines != null)
            {
                if (modelCuisines == null)
                {
                    throw new ArgumentNullException(nameof(modelCuisines));                    
                }
                
                modelCuisinesIds = PlaceConverterUtils.CuisineIdsConverter(clientPatchInfo.Cuisines, modelCuisines);
            }

            var avgCheck = PlaceConverterUtils.AvgCheckConverter(clientPatchInfo.AvgCheck);
            IEnumerable<Model.PlaceWorkingTime> modelWorkingTime = null;

            if (clientPatchInfo.WorkingTime != null)
            {
                if (clientPatchInfo.WorkingTime.Count != 7)
                {
                    throw new InvalidDataException(
                        $"{nameof(clientPatchInfo.WorkingTime)} must contain 7 values.");                    
                }

                modelWorkingTime = PlaceConverterUtils.ConvertWorkingTimeFromClientToModel(clientPatchInfo.WorkingTime);
            }

            var modelPathInfo = new Models.Places.PlacePatchInfo(id)
            {
                Name = clientPatchInfo.Name,
                Avatar = clientPatchInfo.Avatar,
                Description = clientPatchInfo.Description,
                Address = clientPatchInfo.Address,
                Coordinates = clientPatchInfo.Coordinates,
                Phone = clientPatchInfo.Phone,
                Types = modelTypesIds?.ToArray(),
                Cuisines = modelCuisinesIds?.ToArray(),
                AvgCheck = avgCheck,
                WorkingTime = modelWorkingTime?.ToArray()
            };

            return modelPathInfo;
        }
    }
}
