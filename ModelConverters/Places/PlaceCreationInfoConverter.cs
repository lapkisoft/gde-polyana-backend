using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Model = Models.Places;
using Client = ClientModels.Places;

namespace ModelConverters.Places
{
    public static class PlaceCreationInfoConverter
    {
        public static Model.PlaceCreationInfo Convert(Client.PlaceCreationInfo clientCreationInfo, string userId,
            IReadOnlyList<Models.PlaceTypes.PlaceType> modelTypes, 
            IReadOnlyList<Models.PlaceCuisines.PlaceCuisine> modelCuisines)
        {
            if (clientCreationInfo == null)
            {
                throw new ArgumentNullException(nameof(clientCreationInfo));
            }

            if (userId == null)
            {
                throw new ArgumentNullException(nameof(userId));
            }

            if (modelTypes == null)
            {
                throw new ArgumentNullException(nameof(modelTypes));
            }

            if (modelCuisines == null)
            {
                throw new ArgumentNullException(nameof(modelCuisines));
            }

            if (clientCreationInfo.Coordinates == null || clientCreationInfo.Coordinates.Count != 2)
            {
                throw new InvalidDataException(
                    $"{nameof(clientCreationInfo.Coordinates)} must contain 2 values (long and lat).");
            }

            var avgCheck = PlaceConverterUtils.AvgCheckConverter(clientCreationInfo.AvgCheck);
            var modelTypeIds = PlaceConverterUtils.TypeIdsConverter(clientCreationInfo.Types, modelTypes).ToArray();

            if (modelTypeIds == null || !modelTypeIds.Any())
            {
                throw new InvalidDataException(nameof(modelTypeIds));
            }

            IEnumerable<string> modelCuisineIds = null;

            if (clientCreationInfo.Cuisines != null)
            {
                modelCuisineIds = PlaceConverterUtils.CuisineIdsConverter(clientCreationInfo.Cuisines, modelCuisines);
            }

            if (clientCreationInfo.WorkingTime == null || clientCreationInfo.WorkingTime.Count != 7)
            {
                throw new InvalidDataException(
                    $"{nameof(clientCreationInfo.WorkingTime)} must contain 7 values.");
            }
            
            var modelWorkingTime = PlaceConverterUtils.ConvertWorkingTimeFromClientToModel(clientCreationInfo.WorkingTime);

            var modelCreationInfo = new Model.PlaceCreationInfo(clientCreationInfo.Id, clientCreationInfo.Name,
                clientCreationInfo.Description, clientCreationInfo.Address, clientCreationInfo.Coordinates, 
                clientCreationInfo.Phone, modelTypeIds, modelCuisineIds, avgCheck, modelWorkingTime, userId);

            return modelCreationInfo;
        }
    }
}