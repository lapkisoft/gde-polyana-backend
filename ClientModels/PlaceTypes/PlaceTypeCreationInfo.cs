using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace ClientModels.PlaceTypes
{
    [DataContract]
    public class PlaceTypeCreationInfo
    {
        [DataMember(IsRequired = true)]
        [RegularExpression(@"^[A-Za-z0-9_\-]+$", ErrorMessage = "PlaceTypeId")]
        public string Id { get; set; }

        [DataMember(IsRequired = true)]
        public string Name { get; set; }
    }
}