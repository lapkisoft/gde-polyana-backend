﻿using System.Runtime.Serialization;

namespace ClientModels.PlaceTypes
{
    [DataContract]
    public class PlaceTypePatchInfo
    {
        [DataMember(IsRequired = false)]
        public string Name { get; set; }
    }
}
