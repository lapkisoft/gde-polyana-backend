namespace ClientModels.PlaceTypes
{
    public class PlaceType
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }
    }
}