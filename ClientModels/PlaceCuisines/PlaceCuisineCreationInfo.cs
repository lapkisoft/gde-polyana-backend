using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace ClientModels.PlaceCuisines
{
    [DataContract]
    public class PlaceCuisineCreationInfo
    {
        [DataMember(IsRequired = true)]
        [RegularExpression(@"^[A-Za-z0-9_\-]+$", ErrorMessage = "PlaceCuisineId")]
        public string Id { get; set; }
        
        [DataMember(IsRequired = true)]
        public string Name { get; set; }
    }
}