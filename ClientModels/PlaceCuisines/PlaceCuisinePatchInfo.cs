using System.Runtime.Serialization;

namespace ClientModels.PlaceCuisines
{
    [DataContract]
    public class PlaceCuisinePatchInfo
    {
        [DataMember(IsRequired = false)]
        public string Name { get; set; }
    }
}