namespace ClientModels
{
    public class Avatar
    {
        public string Url { get; set; }

        public Avatar(string url)
        {
            Url = url;
        }
    }
}