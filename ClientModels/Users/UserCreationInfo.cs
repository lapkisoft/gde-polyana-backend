﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace ClientModels.Users
{
    [DataContract]
    public class UserCreationInfo
    {
        [DataMember(IsRequired = true)]
        [RegularExpression(@"^[A-Za-z0-9_\-]+$", ErrorMessage = "UserName")]
        public string UserName { get; set; }

        [DataMember(IsRequired = true)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [DataMember(IsRequired = true)]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [DataMember(IsRequired = true)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataMember(IsRequired = true)]
        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
    }
}
