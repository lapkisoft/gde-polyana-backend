﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientModels.Users
{
    public class UserSearchInfo
    {
        /// <summary>
        /// Позиция, начиная с которой нужно производить поиск
        /// </summary>
        public int? Offset { get; set; }

        /// <summary>
        /// Количество заведений, которое нужно вернуть
        /// </summary>
        public int? Limit { get; set; }

        public bool? IsManagerRoleRequested { get; set; }
    }
}
