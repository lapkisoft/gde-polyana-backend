﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;


namespace ClientModels.UserBooking
{
    [DataContract]
    public class UserBookingPatchInfo
    {
        [DataMember(IsRequired = false)]
        public string OrderDate { get; set; }
        
        [DataMember(IsRequired = false)]
        public string OrderTime { get; set; }

        [DataMember(IsRequired = false)]
        public string Status { get; set; }

        [DataMember(IsRequired = false)]
        public int? NumberOfPersons { get; set; }
    }
}
