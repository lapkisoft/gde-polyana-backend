﻿using System.Runtime.Serialization;

namespace ClientModels.UserBooking
{
    [DataContract]
    public class UserBookingCreationInfo
    {
        [DataMember(IsRequired = true)]
        public string PlaceId { get; set; }

        [DataMember(IsRequired = true)]
        public string OrderDate { get; set; }
        
        [DataMember(IsRequired = true)]
        public string OrderTime { get; set; }

        [DataMember(IsRequired = true)]
        public int NumberOfPersons { get; set; }
    }
}
