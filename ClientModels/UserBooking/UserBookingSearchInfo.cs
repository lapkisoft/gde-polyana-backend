namespace ClientModels.UserBooking
{
    public class UserBookingSearchInfo
    {
        /// <summary>
        /// Позиция, начиная с которой нужно производить поиск
        /// </summary>
        public int? Offset { get; set; }

        /// <summary>
        /// Количество заведений, которое нужно вернуть
        /// </summary>
        public int? Limit { get; set; }
        
        public string PlaceId { get; set; }
        public string UserId { get; set; }
        public string Status { get; set; }
    }
}