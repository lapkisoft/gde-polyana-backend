﻿using System;

namespace ClientModels.UserBooking
{
    public class UserBooking
    {
        public string Id { get; set; }
        public string PlaceId { get; set; }
        public string PlaceName { get; set; }
        public string UserId { get; set; }
        public string UserPhone { get; set; }
        public string OrderDate { get; set; }
        public string OrderTime { get; set; }
        public string Status { get; set; }
        public int NumberOfPersons { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime LastUpdateAt { get; set; }
    }
}
