namespace ClientModels.Places
{
    public class PlaceWorkingTime
    {
        public string OpenHourTime { get; set; }
        public string OpenMinuteTime { get; set; }
        public string CloseHourTime { get; set; }
        public string CloseMinuteTime { get; set; }
    }
}