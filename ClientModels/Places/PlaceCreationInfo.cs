using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace ClientModels.Places
{
    [DataContract]
    public class PlaceCreationInfo
    {
        [DataMember(IsRequired = true)]
        [RegularExpression(@"^[A-Za-z0-9_\-]+$", ErrorMessage = "PlaceId")]
        public string Id { get; set; }
        
        [DataMember(IsRequired = true)]
        public string Name { get; set; }
        
        [DataMember(IsRequired = false)]
        public string Description { get; set; }
        
        [DataMember(IsRequired = true)]
        public string Address { get; set; }
        
        [DataMember(IsRequired = true)]
        public IReadOnlyList<double> Coordinates { get; set; }
        
        [DataMember(IsRequired = true)]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }
        
        [DataMember(IsRequired = true)]
        public IReadOnlyList<string> Types { get; set; }

        [DataMember(IsRequired = false)]
        public IReadOnlyList<string> Cuisines { get; set; }

        [DataMember(IsRequired = true)]
        public string AvgCheck { get; set; }
        
        [DataMember(IsRequired = true)]
        public IReadOnlyList<PlaceWorkingTime> WorkingTime { get; set; }
    }
}