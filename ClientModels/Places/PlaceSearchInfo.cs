namespace ClientModels.Places
{
    public class PlaceSearchInfo
    {
        /// <summary>
        /// Позиция, начиная с которой нужно производить поиск
        /// </summary>
        public int? Offset { get; set; }

        /// <summary>
        /// Количество заведений, которое нужно вернуть
        /// </summary>
        public int? Limit { get; set; }

        /// <summary>
        /// Типы заведений
        /// </summary>
        public string[] Type { get; set; }

        /// <summary>
        /// Средний чек
        /// </summary>
        public string[] AvgCheck { get; set; }
        
        /// <summary>
        /// Кухня
        /// </summary>
        public string[] Cuisine { get; set; }
        
        /// <summary>
        /// Идентификатор пользователя, управляющего заведением
        /// </summary>
        public string Manager { get; set; }
    }
}