﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ClientModels.Places
{
    [DataContract]
    public class PlacePatchInfo
    {
        [DataMember(IsRequired = false)]
        public string Name { get; set; }

        [DataMember(IsRequired = false)]
        public string Avatar { get; set; }

        [DataMember(IsRequired = false)]
        public string Description { get; set; }

        [DataMember(IsRequired = false)]
        public string Address { get; set; }
        
        [DataMember(IsRequired = false)]
        public IReadOnlyList<double> Coordinates { get; set; }

        [DataMember(IsRequired = false)]
        public string Phone { get; set; }

        [DataMember(IsRequired = false)]
        public IReadOnlyList<string> Types { get; set; }

        [DataMember(IsRequired = false)]
        public IReadOnlyList<string> Cuisines { get; set; }

        [DataMember(IsRequired = false)]
        public string AvgCheck { get; set; }
        
        [DataMember(IsRequired = false)]
        public IReadOnlyList<PlaceWorkingTime> WorkingTime { get; set; }
    }
}
