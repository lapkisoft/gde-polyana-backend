using System;
using System.Collections.Generic;

namespace ClientModels.Places
{
    public class Place
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Avatar { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public IReadOnlyList<double> Coordinates { get; set; }
        public string Phone { get; set; }
        public IReadOnlyList<string> Types { get; set; }
        public IReadOnlyList<string> Cuisines { get; set; }
        public string AvgCheck { get; set; }
        public IReadOnlyList<PlaceWorkingTime> WorkingTime { get; set; }
        public string Manager { get; set; }
        public string TelegramToken { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime LastUpdateAt { get; set; }
    }
}